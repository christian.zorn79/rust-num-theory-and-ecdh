#[allow(unused_imports)]
use num_bigint::BigUint;
#[allow(unused_imports)]
use std::cmp::Ordering;
#[allow(unused_imports)]
use crate::utils;
#[allow(unused_imports)]
use crate::factoring;

#[test]
fn miller_rabin_test() {
	assert_eq!(factoring::miller_rabin(&BigUint::from(65537u32),20), true);
	assert_eq!(factoring::miller_rabin(&BigUint::from(65535u32),20), false);
	assert_eq!(factoring::miller_rabin(&BigUint::parse_bytes(b"39402006196394479212279040100143613805079739270465446667948293404245721771496870329047266088258938001861606973112319",10).unwrap(), 20), true);
}

#[test]
fn next_prime_test() {
	let n = BigUint::from(0xfeedfacedeadbeefu64);
	let p = factoring::next_prime(&n, 10);
	assert_eq!(p, n+30u32);
}

#[test]
fn generate_safe_prime_tst() {
	let seed = b"Hello World!";
	let p = factoring::generate_safe_prime(128, seed);
	let pp = BigUint::parse_bytes(b"259055138004828552706959697877973978743",10).unwrap();
	assert!(factoring::miller_rabin(&pp,10));
	assert_eq!(p,pp);
	
	let seed = b"Goodnigt Moon!";
	let p = factoring::generate_safe_prime(131,seed);
	let pp = BigUint::parse_bytes(b"691f61813897cb5d9db226c0f88cce52b",16).unwrap();
	assert!(factoring::miller_rabin(&pp,10));
	assert_eq!(p,pp);
}

#[test]
fn lenstra_factoring_test() {
	let n: BigUint = BigUint::from(455839u32);
	let k: BigUint = utils::factorial(20);
	let p: BigUint = BigUint::from(761u32);
	match factoring::lenstra_factoring(&n, &k) {
		None =>  assert_eq!(-1,1),
		Some(q) => {
			match q.cmp(&p){
				Ordering::Equal => assert_eq!(q,p),
				_ => assert_eq!(n,&p*&q)
			}
		}
	};
}

#[test]
fn fermat_factoring_test() {
	let p = BigUint::from(9981899u32);
	let q = BigUint::from(9989899u32);
	let n = &p*&q;
	assert_eq!( factoring::fermat_factoring(&n), Some(p) );
}

#[test]
fn pollard_rho_test() {
	let p = BigUint::from(9981899u32);
	let q = BigUint::from(9989899u32);
	let n = &p*&q;
	assert_eq!( factoring::pollard_rho(&n), Some(p) );
	let nn = BigUint::from(4294967297u64);
	let fac = BigUint::from(641u32);
	assert_eq!( factoring::pollard_rho(&nn), Some(fac));
}

#[test]
fn generate_rsa_key_pair_test() {
	let seed = b"This is a test seed";
	let rsa = factoring::generate_rsa_key_pair(1024, seed).unwrap();
	let p1 = BigUint::parse_bytes(b"c13e35b95efbd681155160f82cfd5632b25fcb80734a99ba3f3761c7d16f50c88cbf13d2a6d742eb501536c30328f3c569d1e1d3dbccf8b25e8f045f77979471", 16).unwrap();
	let p2 = BigUint::parse_bytes(b"d76e922f4f1388b23519a37cfd8bf4857270d1424f16d4b36c864a0f9d87c3bec99617114358a5972d2d025b21a3edabc3981ee15f617acba87cb1fd3bd06045", 16).unwrap();
	let n  = BigUint::parse_bytes(b"a29eb632f52cd7f267e3561b9762723b9ead8ec2ea911ac79be0409681def54d26d02a596fe85f3b3e3a60a917cab6124d3c875f60b260212dd9e1068872b4aad7fadf2374b14dab118995273b7726453d5f093d1e67980207c31f8fda3c9464d09eb4f15c8128023551e26dc3021b184ec724b7a2120038b97ce99fba556275", 16).unwrap();
	let d  = BigUint::parse_bytes(b"7bf6c706ce1ce507d4444846e48f8cb37506b24e28d358624d9f0b7d5f12334884e39e1ef69381f49b76d697bd39ffb4b5f4c09041f3239594c16799dc19d933ba9443670f3c2115f3c1ab0d3c1249471e45ad8e7791075c3d128d0182ee59b448ff04e402564d3b0bc5306ae61ed3d02d6b2b19d5042b220c36392014808647", 16).unwrap();
	assert!(factoring::miller_rabin(&p1,10));
	assert!(factoring::miller_rabin(&p2,10));
	assert_eq!(&p1, &rsa.prime1);
	assert_eq!(&p2, &rsa.prime2);
	assert_eq!(&d, &rsa.decrypt_exp);
	assert_eq!(&n, &rsa.modulus);
}

#[test]
fn rsa_encrypt_decrypt_test() {
	let seed = b"This is a test seed";
	let rsa = factoring::generate_rsa_key_pair(1024, seed).unwrap();
	let pt  = BigUint::from(0xdeadbeeffeedface_u64);
	let ct  = BigUint::parse_bytes(b"1d9b169c2fef6e9ede905a30e308edf180ca463b23e0c573eb813097ccc0f2712e24e1d60b1692bb4bb584f367db25901471820a50129b4a73d7315e12ab62d2659efdc86f8b26605fd7861ae3d42d48b1115021c5cdbbe0f38742673805e90920834cc7eab86b44971b0032a18d9509ad111d42cc3fe018b5110f5a060c75a7",16).unwrap();
	let tmp = rsa.encrypt(&pt);
	assert_eq!(ct,tmp);
	let tmp = rsa.decrypt(&ct);
	assert_eq!(pt,tmp);
}
