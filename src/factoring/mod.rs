use num_bigint::BigUint;
use num_bigint::RandBigInt;
use num::One;
use num::Zero;
use std::cmp::Ordering;
use sha2::{Sha256, Digest};

use crate::utils;
use crate::elliptic_curve::{EcPoint, ec_scalar_multiply};

mod test;

#[derive(Clone,Debug)]
pub enum Error {
	RsaKeySizeError(String),
}

#[derive(Clone,Debug)]
pub struct RsaObject {
	modulus: BigUint,
	encrypt_exp: BigUint,
	prime1: BigUint,
	prime2: BigUint,
	decrypt_exp: BigUint
}

impl RsaObject {
	pub fn encrypt(&self, pt: &BigUint) -> BigUint {
		pt.modpow(&self.encrypt_exp, &self.modulus)
	}

	pub fn decrypt(&self, ct: &BigUint) -> BigUint {
		ct.modpow(&self.decrypt_exp, &self.modulus)
	}
}

/**********************************************************************/
/* A function implementing the Miller-Rabin primality test with       */
/* num_trial iterations (each from a random basepoint)                */
/**********************************************************************/
pub fn miller_rabin(n: &BigUint, num_trials: usize) -> bool{
	let mut res:bool = true;
	let mut i:usize = 0;
	let mr_iter = | n: &BigUint | -> bool {
		let mut rng = rand::thread_rng();
		let low  = BigUint::from(2u32);
		let r    = n - 1u32;
		let high = &r - 1u32;
		let mut x = rng.gen_biguint_range(&low, &high);
		let r = r.trailing_zeros();
		match r {
			None => false,
			Some(rr) => {
				let d = n >> rr;
				x = x.modpow(&d, n);
				let mut res: bool = (x == BigUint::from(1u32)) || (x == n-1u32);
				let mut i = 0u64;
				while i < rr{
					x = (&x*&x) % n;
					res = res || (x == n-1u32);
					i = i+1;
				};
				res
			}
		}		
	};
	while i < num_trials {
		res = res && mr_iter(n);
		i += 1;
		if res == false {
			break;
		}
	}
	res
}

/**********************************************************************/
/* A function that uses miller_rabin with num_trials to find the next */
/* prime starting from n                                              */
/**********************************************************************/
pub fn next_prime(n: &BigUint, num_trials: usize) -> BigUint {
	// Set low bit so that we start with an odd number
	let mut a = (n + 1_u32) | BigUint::from(1u32);
	while !miller_rabin(&a, num_trials) {
		a += 2u32;
	}
	return a
}

pub fn generate_safe_prime(n_bits: usize, seed: &[u8]) -> BigUint {
	let n_bytes = (n_bits + 7) / 8;
	let msb = (n_bits - 1) & 7;
	let mut p_vec = vec![0_u8; n_bytes];
	let fold_sha256_digest = | arr: &[u8] | -> u8 {
		let mut res = 0_u8;
		for i in 0..32{
			res ^= arr[i];
		}
		res
	};
	let mut digest = sha2::Sha256::digest(seed);
	for i in 0..n_bytes{
		digest = sha2::Sha256::digest(&digest);
		p_vec[i] = fold_sha256_digest(&digest);
	}
	p_vec[0]  |= 0x80; // Set the top bit
	p_vec[0] >>= 7 - msb + 1; // Shift down the msb to to generate
	                          // a n_bits-1 prime to double and add 1
	if p_vec[0] == 0 {
		p_vec[1] |= 0x80; // If p_vec[0] is shifted off, set msb of p_vec[1]
	}
	p_vec[n_bytes-1] |= 0x01; // Set lsb to 1 (making p_vec rep a odd #)
	let mut q = next_prime(&BigUint::from_bytes_be(&p_vec),10);
	let mut p = 2_u32*&q + 1_u32;
	while !miller_rabin(&p, 10) {
		q = next_prime(&q, 10);
		p = 2_u32*&q + 1_u32;
	}
	p
}


/**********************************************************************/
/* A function attemption to use Lenstra's elliptic curve              */
/* factorization to find a factor of n.  It computes kP for a random  */
/* point (x0,y0) on a random elliptic curve y^2 = x^3 + a*x + b over  */
/* ZZ/nZZ.  The b is computed from x0,y0, and a                       */
/**********************************************************************/
pub fn lenstra_factoring(n: &BigUint, k: &BigUint) -> Option<BigUint> {
	let mut rng = rand::thread_rng();
	let a  = rng.gen_biguint_range(&BigUint::zero(), n);
	let x0 = rng.gen_biguint_range(&BigUint::zero(), n);
	let y0 = rng.gen_biguint_range(&BigUint::zero(), n);
	//let b  = &y0*&y0 + (n  - ((&x0 * &x0 * &x0) % n)) + (n - ((&a * &x0) % n)) % n;
	let pt = EcPoint(x0, y0, false);
	let out = ec_scalar_multiply(&pt, &k, &a, &n);
	match out {
		Err(utils::Error::NonUnitGCD(g)) => Some(g),
		_ => None
	} 
} 

/**********************************************************************/
/* A function to factor the BigUint n using Fermat factoring.         */
/* Because Fermat factoring can fail, it returns an Option<BigUint>   */
/* either None when no factor is found or Some(factor) for some       */
/* non-trivial factor of n                                            */
/**********************************************************************/
pub fn fermat_factoring(n: &BigUint) -> Option<BigUint> {
	let n_lw = n.to_bytes_le()[0];
	if n_lw & 1 == 0  {
		panic!("Input to fermat_factoring should be odd");
	}
	let mut a = utils::floor_sqrt_biguint(n);
	match n.cmp(&(&a * &a)) {
		Ordering::Equal => Some(a),
		_ => {
			loop {
				a += 1u32;
				let b2 = &a*&a - n;
				if utils::is_square_biguint(&b2){
					let b = utils::floor_sqrt_biguint(&b2);
					break Some(&a-&b)
				}
				if n.cmp(&a) == Ordering::Equal {
					break None;
				}	
			}
		}
	}
}

/**********************************************************************/
/* Function to find a factor of the BigUint n.  Because Pollard rho   */
/* can fail, we have it return an Option<BigUint>                     */
/**********************************************************************/
pub fn pollard_rho(n: &BigUint) -> Option<BigUint> {
	// Initialize values x, x_fixed, fac, and size
	let mut x:BigUint = BigUint::from(2u32);
	let mut x_fixed:BigUint = BigUint::from(2u32);
	let mut fac: BigUint = BigUint::one();
	let mut size = 2i32;
	loop {
		// Set count to size
		let mut count = size;
		// Loop as long as count>0 and fac == 1
		while (count > 0) && (fac.cmp(&BigUint::one()) == Ordering::Equal) {
			// Update x
			x = (&x * &x + 1u32) % n;
			// Compute x-x_fixed mod n
			let z = (&x + n - &x_fixed) % n;
			// Update fac = gcd(x - x_fixed % n, n)
			fac = utils::gcd(&z, n).unwrap();
			// Decrement count
			count -= 1;
		}
		match (fac.cmp(&BigUint::one()),fac.cmp(n)) {
			(Ordering::Equal,_) => {
				// Case where we want to re-enter the loop
				size *= 2;
				x_fixed = x.clone();
				},
			(_,Ordering::Equal) => break None,
			(_,_) => break Some(fac)
		}
	}
}

pub fn generate_rsa_key_pair(key_size: usize, seed: &[u8]) -> Result<RsaObject,Error> {
	let num_bytes_primes = match key_size {
		1024 | 2048 | 4096 => key_size/16,
		_ => return Err(Error::RsaKeySizeError(format!("generate_rsa_key_pair key_size: {} (should be 1024, 2048, 4096)", key_size)))
	};
	let mut p1_vec = vec![0_u8; num_bytes_primes];
	let mut p2_vec = vec![0_u8; num_bytes_primes];
	let e = BigUint::from(0x65537_u32); // Encryption exponent 2**16+1
	// A PRNG for bytes for our primes
	let fold_sha256_digest = | arr: &[u8] | -> u8 {
		let mut res = 0_u8;
		for i in 0..32{
			res ^= arr[i];
		}
		res
	};
	// Generate the random for the primes
	let mut digest = Sha256::digest(seed);
	for i in 0..num_bytes_primes{
		digest = sha2::Sha256::digest(&digest);
		p1_vec[i] = fold_sha256_digest(&digest);
		digest = sha2::Sha256::digest(&digest);
		p2_vec[i] = fold_sha256_digest(&digest);
	}
	// Set top two bits and low bit
	p1_vec[0] |= 0xc0;
	p2_vec[0] |= 0xc0;
	p1_vec[num_bytes_primes-1] |= 1;
	p2_vec[num_bytes_primes-1] |= 1;
	// Generate the net prime that is not 1 % e
	let mut p1 = next_prime(&BigUint::from_bytes_be(&p1_vec),10);
	let mut p2 = next_prime(&BigUint::from_bytes_be(&p2_vec),10);
	while &p1 % &e == BigUint::one() {
		p1 = next_prime(&p1,10);
	}
	while &p2 % &e == BigUint::one() {
		p2 = next_prime(&p2, 10);
	}
	// At this point we can compute the public modulus and private
	// exponent
	let n = &p1 * &p2;
	let phi_n = &(&p1 - 1_u32) * &(&p2 - 1_u32);
	let d = utils::mod_inverse(&e, &phi_n).unwrap();
	let rsa = RsaObject{ 
		modulus:n, 
		encrypt_exp:e, 
		prime1:p1, 
		prime2:p2, 
		decrypt_exp:d };
	Ok(rsa)


}
