#[allow(unused_imports)]
use num_bigint::BigUint;
#[allow(unused_imports)]
use std::cmp::Ordering;
#[allow(unused_imports)]
use crate::elliptic_curve::{EcPoint, ec_double, ec_add, ec_scalar_multiply, ec_verify_point, ec_find_y_coord};

#[test]
fn ec_verify_point_test(){
	let p = BigUint::parse_bytes(b"39402006196394479212279040100143613805079739270465446667948293404245721771496870329047266088258938001861606973112319",10).unwrap();
	let x = EcPoint( BigUint::parse_bytes(b"aa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7", 16).unwrap(),
					 BigUint::parse_bytes(b"3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f", 16).unwrap(),
					 false);
	let b = BigUint::parse_bytes(b"b3312fa7e23ee7e4988e056be3f82d19181d9c6efe8141120314088f5013875ac656398d8a2ed19d2a85c8edd3ec2aef", 16).unwrap();
	assert!(ec_verify_point(&x, &(&p - 3u32), &b, &p));
}

#[test]
fn ec_double_test(){
	let p = BigUint::parse_bytes(b"39402006196394479212279040100143613805079739270465446667948293404245721771496870329047266088258938001861606973112319",10).unwrap();
	let x = EcPoint( BigUint::parse_bytes(b"aa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7", 16).unwrap(),
					 BigUint::parse_bytes(b"3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f", 16).unwrap(),
					 false);
	let y = ec_double(&x, &(&p-3u32), &p).unwrap();
	assert_eq!(y.0, BigUint::parse_bytes(b"08D999057BA3D2D969260045C55B97F089025959A6F434D651D207D19FB96E9E4FE0E86EBE0E64F85B96A9C75295DF61", 16).unwrap());
	assert_eq!(y.1, BigUint::parse_bytes(b"8E80F1FA5B1B3CEDB7BFE8DFFD6DBA74B275D875BC6CC43E904E505F256AB4255FFD43E94D39E22D61501E700A940E80", 16).unwrap());
	assert_eq!(y.2, false);
	
	let xx = EcPoint( BigUint::from(0u32), BigUint::from(1u32), true );
	let yy = ec_double(&xx, &(&p-3u32), &p).unwrap();
	assert_eq!(yy.0, BigUint::from(0u32));
	assert_eq!(yy.1, BigUint::from(1u32));
	assert_eq!(yy.2, true);
}

#[test]
fn ec_add_test(){
	let p = BigUint::parse_bytes(b"39402006196394479212279040100143613805079739270465446667948293404245721771496870329047266088258938001861606973112319",10).unwrap();
	let x = EcPoint( BigUint::parse_bytes(b"aa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7", 16).unwrap(),
					 BigUint::parse_bytes(b"3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f", 16).unwrap(),
					 false);
	let y = EcPoint( BigUint::parse_bytes(b"08D999057BA3D2D969260045C55B97F089025959A6F434D651D207D19FB96E9E4FE0E86EBE0E64F85B96A9C75295DF61", 16).unwrap(),
	                 BigUint::parse_bytes(b"8E80F1FA5B1B3CEDB7BFE8DFFD6DBA74B275D875BC6CC43E904E505F256AB4255FFD43E94D39E22D61501E700A940E80", 16).unwrap(),
	                 false);
	let z = ec_add(&x, &y, &(&p-3u32), &p).unwrap();
	assert_eq!(z.0, BigUint::parse_bytes(b"077A41D4606FFA1464793C7E5FDC7D98CB9D3910202DCD06BEA4F240D3566DA6B408BBAE5026580D02D7E5C70500C831", 16).unwrap());
	assert_eq!(z.1, BigUint::parse_bytes(b"C995F7CA0B0C42837D0BBE9602A9FC998520B41C85115AA5F7684C0EDC111EACC24ABD6BE4B5D298B65F28600A2F1DF1", 16).unwrap());
	assert_eq!(z.2, false);
}

#[test]
fn ec_scalar_multiply_test(){
	let p = BigUint::parse_bytes(b"39402006196394479212279040100143613805079739270465446667948293404245721771496870329047266088258938001861606973112319",10).unwrap();
	let x = EcPoint( BigUint::parse_bytes(b"aa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7", 16).unwrap(),
					 BigUint::parse_bytes(b"3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f", 16).unwrap(),
					 false);
	let n = BigUint::parse_bytes(b"39402006196394479212279040100143613805079739270465446667946905279627659399113263569398956308152294913554433653942642", 10).unwrap();
	let y = ec_scalar_multiply(&x, &n, &(&p-3u32), &p).unwrap();	
	assert_eq!(y.0, BigUint::parse_bytes(b"AA87CA22BE8B05378EB1C71EF320AD746E1D3B628BA79B9859F741E082542A385502F25DBF55296C3A545E3872760AB7", 16).unwrap());
	assert_eq!(y.1, BigUint::parse_bytes(b"C9E821B569D9D390A26167406D6D23D6070BE242D765EB831625CEEC4A0F473EF59F4E30E2817E6285BCE2846F15F1A0", 16).unwrap());
	assert_eq!(y.2, false);
}

#[test]
fn ec_find_y_coord_test() {
	let p = BigUint::parse_bytes(b"39402006196394479212279040100143613805079739270465446667948293404245721771496870329047266088258938001861606973112319",10).unwrap();
	let x = BigUint::parse_bytes(b"aa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7", 16).unwrap();
	let a = &p - 3u32;
	let b = BigUint::parse_bytes(b"b3312fa7e23ee7e4988e056be3f82d19181d9c6efe8141120314088f5013875ac656398d8a2ed19d2a85c8edd3ec2aef", 16).unwrap();
	let tgt0 = BigUint::parse_bytes(b"3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f", 16).unwrap();
	let tgt1 = &p - &tgt0;
	let y = ec_find_y_coord(&x, &a, &b, &p);
	match y {
		None => assert!(false),
		Some(yy) => {
			match (tgt0.cmp(&yy.1), tgt1.cmp(&yy.1)) {
				(Ordering::Equal, _) => assert!(true),
				(_, Ordering::Equal) => assert!(true),
				_ => assert!(false)
			}
		}
	}
}
