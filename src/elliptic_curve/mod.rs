use num_bigint::BigUint;
use std::cmp::Ordering;
use crate::utils::{mod_inverse,tonelli_shanks,Error as UtilError};

mod test;

#[derive(Clone,Debug,PartialOrd)]
pub struct EcPoint(pub BigUint, pub BigUint, pub bool);

impl PartialEq for EcPoint{
	fn eq(&self, other: &Self) -> bool{
		(self.0 == other.0) && (self.1 == other.1) && (self.2 == other.2)
	}
}

/**********************************************************************/
/* A funciton to verify that the point x = (x.0,x.1,x.2) belongs to   */
/* the elliptic curve y^2 = x^3 + ax + b modulo p.                    */
/**********************************************************************/
pub fn ec_verify_point(x: &EcPoint, a: &BigUint, b: &BigUint, p: &BigUint) -> bool{
	x.2 || ( ((&x.1 * &x.1) % p) == ((&x.0*&x.0*&x.0 + &x.0*a + b) % p) )
}


/**********************************************************************/
/* A function to double a point a on an elliptic curve of the form    */
/* y^2 = x^3 + alpha * x + _beta mod p.                               */
/* Note that this formula does not depend on _beta                    */
/**********************************************************************/
pub fn ec_double(a: &EcPoint, alpha: &BigUint, p: &BigUint) -> Result<EcPoint, UtilError>{
	match a.2 {
		// Case of a being Id
		true => Ok(EcPoint(BigUint::from(0u32),BigUint::from(1u32),true)),
		 _   => { 
			 match a.1.cmp(&BigUint::from(0u32)) {
				 // Case of point of order 2
				 Ordering::Equal => Ok(EcPoint(BigUint::from(0u32),BigUint::from(1u32), true)),
				 // Generic case
				 _    => {
					let mut m = mod_inverse(&(&a.1*2u32), p)?; // Try to invert 2y_0 mod p
					m = (&m * (&a.0*&a.0*3u32 + alpha)) % p; // Slope of the tangent line
					let x_1 = (&m*&m + (p - &a.0)*2u32) % p; // x_1 = m^2 - 2x_0
					let y_1 = (&m*(&x_1 + (p - &a.0)) + &a.1) % p; // y_1 = m(x_1 - x_0) + y_0
					Ok(EcPoint(x_1, p - y_1, false))		
				}
			}
		}
	}
}

/**********************************************************************/
/* A function to add two points a and b on an elliptic curve of the   */
/* form y^2 = x^3 + alpha * x + _beta mod p.                          */
/* Note that this formula does not depend on _beta                    */
/**********************************************************************/
pub fn ec_add(a: &EcPoint, b: &EcPoint, alpha: &BigUint, p: &BigUint) -> Result<EcPoint, UtilError>{
	match (a.2,b.2) {
		// The top two cases cover the case of either point being Id
		(_,true) => Ok(EcPoint(a.0.clone(),a.1.clone(),a.2)),				
		(true,_) => Ok(EcPoint(b.0.clone(),b.1.clone(),b.2)),
		_ => {
			match (a.0==b.0,a.1==b.1){
				// If the points are the same, double one of them
				(true,true)  => ec_double(a,alpha,p), 
				// If the points are inverses (i.e., have the from
				// (x,y) and (x,-y)
				(true,false) => Ok(EcPoint(BigUint::from(0u32), BigUint::from(1u32), true)),
				// The generic case
				_ =>{
					let mut m = mod_inverse(&(&b.0 + (p -&a.0)),p)?;
					m = (&m * (&b.1 + (p - &a.1)) )% p; // Slope of the secant line
					let x_2 = (&m*&m + (p - &a.0) + (p - &b.0)) % p;  //x_2 = m^2 - x_0 - x_1
					let y_2 = (&m*(&x_2 + (p - &a.0)) + &a.1) % p;
					Ok(EcPoint(x_2, p - y_2, false))
				}
			}
		}
	}
}

/**********************************************************************/
/* Scalar multiplication of a point a on an elliptic curve of the     */
/* y^2 = x^3 + alpha*x + _beta by the scalar n using the Method of    */
/* Russian Peasants.                                                  */
/* Note that this formula does not depend on _beta                    */
/**********************************************************************/ 
pub fn ec_scalar_multiply(a: &EcPoint, n: &BigUint, alpha: &BigUint, p :&BigUint) -> Result<EcPoint, UtilError>{
	let mut b = EcPoint( BigUint::from(0u32), BigUint::from(1u32), true) ;
	// now we will use the Method of Russian Peasants to compute this 
	let n_bytes = n.to_bytes_be();
	for d in n_bytes {
		let mut i:i8 = 7;
		while i >= 0{
			b = ec_double(&b, alpha, p)?;
			if (d >> i) & 1 != 0{
				b = ec_add(&b, a, alpha, p)?;
			}
			i -= 1;
		}
	}
	Ok(b)
}

/**********************************************************************/
/* Given an x value, ec_find_y_coord attempts to find a point on the  */
/* the elliptic curve y^2 = x^3 + ax + b modulo p.                    */
/**********************************************************************/
pub fn ec_find_y_coord(x: &BigUint, a: &BigUint, b: &BigUint, p: &BigUint) -> Option<EcPoint>{
	let y_sq = (x*x*x + a*x + b) % p;
	match tonelli_shanks(&y_sq, p){
		None => None,
		Some(y) => Some(EcPoint(x.clone(), y, false))
	}
}
