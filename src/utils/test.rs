#[allow(unused_imports)]
use num_bigint::BigUint;
#[allow(unused_imports)]
use num::One;
#[allow(unused_imports)]
use crate::utils;

#[test]
fn eratosthenes_test(){
	let is_prime = utils::eratosthenes(99);
	let mut  primes: Vec<u64> = Vec::new();
	for i in 1..100{
		if is_prime[i]{
			primes.push(i as u64);
		}
	}
	let target: Vec<u64> = vec![2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97];
	assert_eq!(primes, target);
	let is_prime2 = utils::eratosthenes(101);
	let mut  primes2: Vec<u64> = Vec::new();
	for i in 1..102{
		if is_prime2[i]{
			primes2.push(i as u64);
		}
	}
	let target2: Vec<u64> = vec![2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101];
	assert_eq!(primes2, target2);
}

#[test]
fn fibonacci_test(){
	let a = utils::fibonacci(98);
	let b = BigUint::parse_bytes(b"218922995834555169026",10).unwrap();
	assert_eq!(a,b);
}


#[test]
fn floor_avg_test(){
	assert_eq!(utils::floor_avg(250u64, 262u64), 256u64);
	assert_eq!(utils::floor_avg(0xffffffffffffffffu64, 0xffffffffffffff00u64), 0xffffffffffffff7fu64); 
}

#[test]
fn floor_sqrt_test(){
	assert_eq!(utils::floor_sqrt(65535), 255u64);
	assert_eq!(utils::floor_sqrt(65536), 256u64);
	assert_eq!(utils::floor_sqrt(65537), 256u64);
}

#[test]
fn floor_sqrt_biguint_test(){
	let a = BigUint::from(0xdeadbeeffeedfaceu64);
	let a2 = &a*&a + 1u32;
	assert_eq!(utils::floor_sqrt_biguint(&a2), a );
}

#[test]
fn is_square_biguint_test() {
	let a = BigUint::from(0xdeadbeeffeedfaceu64);
	let a2 = &a*&a;
	assert_eq!(utils::is_square_biguint(&a2), true );
	let b  = &a2 + 1u32;
	assert_eq!(utils::is_square_biguint(&b), false );
}

#[test]
fn gcd_test() {
	let a: BigUint = BigUint::from(65535u32);
	let b: BigUint = BigUint::from(30840u32);
	let g: BigUint = BigUint::from(3855u32);
	assert_eq!(utils::gcd(&a,&b).unwrap(),g);
	let aa: BigUint = BigUint::from(65537u32);
	let bb: BigUint = BigUint::from(17u32*19u32*23u32*29u32);
	let gg: BigUint = BigUint::one();
	assert_eq!(utils::gcd(&aa,&bb).unwrap(),gg);
}

#[test]
fn jacobi_symbol_neg1_test() {
	assert_eq!(utils::jacobi_symbol_neg1(&BigUint::from(101u32)).unwrap(), 1i8);
	assert_eq!(utils::jacobi_symbol_neg1(&BigUint::from(103u32)).unwrap(), -1i8);
}

#[test]
fn jacobi_symbol_2_test() {
	assert_eq!(utils::jacobi_symbol_2(&BigUint::from(101u32)).unwrap(),-1i8);
	assert_eq!(utils::jacobi_symbol_2(&BigUint::from(103u32)).unwrap(), 1i8);
}

#[test]
fn jacobi_symbol_test() {
	assert_eq!(utils::jacobi_symbol(&BigUint::from(2u32),&BigUint::from(7u32)).unwrap(), 1i8);
	assert_eq!(utils::jacobi_symbol(&BigUint::from(3u32),&BigUint::from(7u32)).unwrap(), -1i8);
	// (31,101) = (101,31) = (8, 31) = (2, 31) = 1
	assert_eq!(utils::jacobi_symbol(&BigUint::from(31u32),&BigUint::from(101u32)).unwrap(), 1i8);
	// (103, 531) = -1 * (531, 103) = -1 * (16, 103) = -1 * (4, 103) = -1
	assert_eq!(utils::jacobi_symbol(&BigUint::from(103u32), &BigUint::from(531u32)).unwrap(), -1i8);
	// (3, 41) = (41, 3) = (2, 3) = -1
	assert_eq!(utils::jacobi_symbol(&BigUint::from(3u32), &BigUint::from(41u32)).unwrap(), -1i8);
}


#[test]
fn xgcd_test(){
	let a = BigUint::parse_bytes(b"39402006196394479212279040100143613805079739270465446667948293404245721771496870329047266088258938001861606973112319", 10).unwrap();
	let b = BigUint::from(65537u32);
	let (s,t,g) = utils::xgcd(&a, &b).unwrap();
	let gg = &s*&a - &t*&b;
	assert_eq!(g, gg);
}





#[test]
fn mod_inverse_test(){
	let p  = BigUint::parse_bytes(b"39402006196394479212279040100143613805079739270465446667948293404245721771496870329047266088258938001861606973112319", 10).unwrap();
	let a  = BigUint::from(65537u32);
	let ai = utils::mod_inverse(&a, &p).unwrap();
	assert_eq!( (a*ai) % p, BigUint::one());
	let q  = BigUint::from(91u32);
	let b  = BigUint::from(26u32);
	if let Err(utils::Error::NonUnitGCD(bi)) = utils::mod_inverse(&b, &q){
		assert_eq!(bi, BigUint::from(13u32));
	}
	else{
		assert!(false);
	}
}

#[test]
fn powmod_oddmod_tst() {
	let m = BigUint::from(531_u32);
	let a = BigUint::from(441_u32);
	let e = BigUint::from(125_u32);
	let b = utils::powmod_oddmod(&a, &e, &m).unwrap();
	let tgt = a.modpow(&e, &m);
	assert_eq!(b,tgt);
}

#[test]
fn tonelli_shanks_test() {
	let a = BigUint::from(2u32);
	let p = BigUint::from(7u32);
	let r = utils::tonelli_shanks(&a, &p).unwrap();
	assert_eq!( (&r * &r) % p, a);
	
	let a = BigUint::from(5u32);
	let p = BigUint::from(41u32);
	let r = utils::tonelli_shanks(&a, &p).unwrap();
	assert_eq!( (&r * &r) % p, a);
	
	let a = BigUint::from(7u32);
	let p = BigUint::from(41u32);
	let r = utils::tonelli_shanks(&a, &p);
	assert_eq!(r, None); 
}
