use num_bigint::BigUint;
use num::One;
use num::Zero;
use std::mem::replace;
use std::mem::swap;
use std::cmp::Ordering;

mod test;

#[derive(Clone,Debug)]
pub enum Error {
	InvalidArguments(String),
	NonUnitGCD(BigUint),
}

pub fn is_odd(a: &BigUint) -> bool {
	let lob = a.to_bytes_le()[0];
	(lob & 1) == 1
}

/**********************************************************************/
/* Computes a vector of bool is_prime of size n+1 for a uint64 n so   */
/* that the is_prime[i] is the answer to the query is i prime?        */
/**********************************************************************/
pub fn eratosthenes(n: u64) -> Vec<bool>{
	let mut is_prime: Vec<bool> = vec![true; (n+1) as usize];
	is_prime[0] = false;
	is_prime[1] = false;
	for i in 2..floor_sqrt(n){
		for j in 2..((n/i)+1){
			is_prime[(i*j) as usize] = false;
		}
	}
	is_prime
}

pub fn factorial(n:u64) -> BigUint {
	match n {
		0 | 1 => BigUint::from(1_u32),
		_ => BigUint::from(n) * factorial(n-1)
	}
}

pub fn fibonacci(n:u64) -> BigUint {
	let mut r_old = BigUint::from(1_u32);
	let mut r_new = BigUint::from(1_u32);
	for _i in 1..n {
		let r = &r_old + &r_new;
		r_old = replace(&mut r_new, r);
	}
	r_new		
}

/**********************************************************************/
/* Computes the integer part of (a+b)/2 for two arguments a, b        */
/**********************************************************************/ 
fn floor_avg(a: u64, b: u64) -> u64 {
   (( (a as u128) + (b as u128) ) / 2) as u64
}


/**********************************************************************/
/* Computes the integer part of sqrt(n) for an argument n             */
/**********************************************************************/ 
pub fn floor_sqrt(n: u64) -> u64 {
   let mut a_old: u64;
   let mut a_new: u64 = n;
   loop {
	   
	   a_old = a_new;
	   a_new = floor_avg(a_old, n / a_old);
	   if a_new >= a_old {
		   break a_old;
	   }
   }
}

/**********************************************************************/
/* Computes the integer part of sqrt(n) for a BigUint n               */
/**********************************************************************/ 
pub fn floor_sqrt_biguint(n: &BigUint) -> BigUint {
	let mut a_new = n.clone();
	loop {
		// Compute the integer part of the averate of a_new and n/a_mnew
		let a = (&a_new + n/&a_new)/2u32;
		let a_old = replace(& mut a_new, a);
		match a_new.cmp(&a_old) {
			Ordering::Less => continue,
			_ => break a_old
		}
	}
}


/**********************************************************************/
/* Computes a bool for whether the given BigUint n is a square.  It   */
/* computes a = floor_sqrt_biguint(n) and then sees if a*a == n       */
/**********************************************************************/
pub fn is_square_biguint(n: &BigUint) -> bool {
	let fsq_n = floor_sqrt_biguint(n);
	let check = &fsq_n * &fsq_n;
	match n.cmp(&check) {
		Ordering::Equal => true,
		_ => false
	}
}

/**********************************************************************/
/* A vanilla gcd for BigUints a and b.  It does not recover scalars   */
/* x and y for which ax + by = gcd(a,b), but it does execute much     */
/* quicker than xgcd that does recover x and y                        */
/**********************************************************************/
pub fn gcd(a: &BigUint, b: &BigUint) -> Result<BigUint,Error>  {
	let mut r_0: BigUint = a.clone();
	let mut r_1: BigUint = b.clone();
	match (r_0.cmp(&Zero::zero()), r_1.cmp(&Zero::zero())){
		(Ordering::Equal,Ordering::Equal) => Err(Error::InvalidArguments("Two zero inputs to gcd".to_string())),
		(Ordering::Equal,_) => Ok(r_1),
		(_,Ordering::Equal) => Ok(r_0),
		_ => {
			loop {
				let q = &r_0/&r_1;
				let r = &r_0 - &q*&r_1;
				r_0   = replace(&mut r_1,r);
				if r_1 == Zero::zero(){
					break Ok(r_0)
				}
			}
		}
	}	
}


/**********************************************************************/
/* A function to compute the Jacobi symbol (-1,a) where a is an odd   */
/* integer.  This is given by the formula (-1)^((a-1)/2),             */
/* which is 1 for a = 1 % 4 and -1 for a = 3 % 4                      */
/**********************************************************************/
pub fn jacobi_symbol_neg1(a: &BigUint) -> Result<i8,Error> {
	let lob: u8 = (a.to_bytes_le())[0]; // Grab the low order byte
	match lob & 3 { // Branch on value of lob mod 4
		1 => Ok(1),
		3 => Ok(-1),
		_ => Err(Error::InvalidArguments(format!("Argument to jacobi_symbol_neg1: {} (should be odd", a))) 
	}
}

/**********************************************************************/
/* A function to compute the Jacobi symbol (2,a) where a is an odd    */
/* integer.  This is given by the formula (-1)^((a^2-1)/8),           */
/* which is 1 for a = +/-1 % 8 and -1 for a = +/-3 % 8                */
/**********************************************************************/
pub fn jacobi_symbol_2(a: &BigUint) -> Result<i8,Error> {
	let lob: u8 = (a.to_bytes_le())[0]; // Grag the low order byte
	match lob & 7 { // Break into cases mod 8
		1 | 7 => Ok(1),
		3 | 5 => Ok(-1),
		_ => Err(Error::InvalidArguments(format!("Argument to jacobi_symbol_2: {} (should be odd", a))) 
	}
}


/**********************************************************************/
/* A function to compute the Jacobi symbol (a,b) where b is an odd    */
/* integer.  It will return 0 if gcd(a,b) > 1                         */
/**********************************************************************/
pub fn jacobi_symbol(a: &BigUint, b: &BigUint) -> Result<i8,Error> {
	if a.cmp(&BigUint::zero()) == Ordering::Equal {
		Ok(1)
	}
	else if a.cmp(&BigUint::one()) == Ordering::Equal {
		Ok(1)
	}
	else if a.cmp(&BigUint::from(2_u32)) == Ordering::Equal {
		jacobi_symbol_2(b)
	}
	else if a.cmp(&(b - 1_u32)) == Ordering::Equal {
		jacobi_symbol_neg1(b)
	}
	else{
		// The recursive cases
		if a.cmp(b) == Ordering::Greater {
			// If a > b, reduce a mod b
			jacobi_symbol(&(a % b), b)
		}
		else{
			// If a= 2^ka', then (a,b) = (2,b)^k(a,b)
			let t = a.trailing_zeros().unwrap();
			if t > 0 {
				let js  = jacobi_symbol(&(a >> t), b)?;
				if t & 1 == 0 {
					Ok(js)
				}
				else{
					let js2 = jacobi_symbol_2(b)? * js;
					Ok(js2)
				}
			}
			else{
				// Finally a and b are odd, so use QR
				let a_mod4 = a.to_bytes_le()[0] & 3;
				let b_mod4 = b.to_bytes_le()[0] & 3;
				let js = jacobi_symbol(b, a)?;
				let res = if (a_mod4 == 1) || (b_mod4 == 1) { Ok(js) } else { Ok(-js) };
				res
			}
		}
	}
}



/**********************************************************************/
/* An algorithm to compute the Extended Euclidean Algorithm for       */
/* BigUints a and b.  It returns a triple (x, y, g) so that           */
/* g = gcd(a,b) and ax - by = g                                         */
/**********************************************************************/
pub fn xgcd(a: &BigUint, b: &BigUint) -> Result<(BigUint, BigUint, BigUint),Error> {
	let mut r_0: BigUint   = a.clone();
	let mut r_1: BigUint   = b.clone();
	if r_0 == Zero::zero(){
		return Err(Error::InvalidArguments(format!("Invalid argument to xgcd : {}",r_0)));
	}
	if r_1 == Zero::zero(){
		return Err(Error::InvalidArguments(format!("Invalid argument to xgcd : {}",r_1)));
	}
	let mut y_old: BigUint = Zero::zero();
	let mut y_new: BigUint = One::one();
	let mut x_old: BigUint = One::one();
	let mut x_new: BigUint = Zero::zero();

	let mut det:i8 = 1;
	while r_1 != Zero::zero() {
		let q = &r_0/&r_1;
		let r = &r_0 - &q*&r_1;
		r_0   = replace(&mut r_1, r);
		let x = &x_old + &q*&x_new;
		x_old = replace(&mut x_new, x);
		let y = &y_old + &q*&y_new;
		y_old = replace(&mut y_new, y);
		det *= -1;
	}
	match det {
		1 => Ok((x_old, y_old, r_0)),
		_ => Ok((b - &x_old, a - &y_old, r_0))
	}
}

/**********************************************************************/
/* A function to compute the inverse of a modulo n.  It returns an    */
/* an Option<BigUint>.  If gcd(a,n) > 1, then it returns None,        */
/* otherwise it returns a BigUint ai so that a*ai = 1 mod n           */
/**********************************************************************/
pub fn mod_inverse(a: &BigUint, n: &BigUint) -> Result<BigUint, Error> {
	let (s, _t, g) = xgcd(a, n)?;
	match g.cmp(&BigUint::from(1u32)) {
		Ordering::Equal => Ok(s),
		_ => Err(Error::NonUnitGCD(g))
	}
}


pub fn powmod_oddmod(a: &BigUint, e: &BigUint, m: &BigUint) -> Result<BigUint,Error>{
	// Set up the additional values needed to perform
	// Montgomery multiplication
	if !is_odd(m){
		return Err(Error::InvalidArguments(format!("Third input to powmod_oddmod {} (should be odd)", m)));
	}
	let m_bytes = m.to_bytes_be();
	let mut msb = 8*(m_bytes.len()-1);
	let mut w = m_bytes[0];
	while w != 0 {
		msb += 1;
		w >>= 1;
	}
	let mut r_vec = vec![0_u8; msb/8 + 1];
	r_vec[0] = 1_u8 << (msb & 7);
	let r  = BigUint::from_bytes_be(&r_vec);
	let (_,mi,_) = xgcd(&r, m)?; 
	let rm = &r - 1_u32;
	// Create a closure for the multiplication step
	let mont_mul = |xbar: &BigUint, ybar: &BigUint| -> BigUint {
		let s = xbar*ybar; 
		let t = (&s * &mi) & &rm; // x & rm = x % r (r = 2^N)
		let u = (&s + &t * m) >> msb; // x >> msb = x / r (r = 2^n)
		match u.cmp(&m) {
			Ordering::Less => u,
			_ => &u - m
		}
	};
	// Now we can do the actual exponentiation using the method
	// of Russian peasants
	let abar  = (a * &r) % m;
	let mut x = &r % m;
	let e_bytes = e.to_bytes_be();
	for byte in e_bytes {
		let mut i = 7_i32;
		while i >= 0 {
			x = mont_mul(&x, &x);
			if (byte >> i) & 1 == 1 {
				x = mont_mul(&x, &abar);
			}
			i -= 1;
		}
	}
	Ok(mont_mul(&x, &BigUint::one()))
}

/**********************************************************************/
/* Function to try and compute the sqrt of a modulo p.  It returns    */
/* Some(r) for a sqrt if it exists; and None if a has not sqrt mod p. */
/* This function utilized the Tonelli-Shanks algorithm and assumes    */
/* that p has been primality tested.  It does *no* primality testing  */
/* on p.                                                              */
/**********************************************************************/
pub fn tonelli_shanks(a: &BigUint, p: &BigUint) -> Option<BigUint> {
	// So as not to do primality checking everytime this is run,
	// we will assume that the user primality checked p prior to calling
	let s = BigUint::trailing_zeros(&(p - 1u32)).unwrap();
	let mut q = p >> (s as usize);
	match s {
		1 => { // Case that p = 3 % 4
			q = (q + 1u32) / 2u32;
			let r = BigUint::modpow(a, &q, p);
			match a.cmp(&((&r * &r) % p)) {
				Ordering::Equal => Some(r), // In this case, r is the square root
				_ => None // In this case a is a non-residue
			} 	
		},
		_	=> { // Case that p = 1 % 4
			// Next we need to find a non-residue mod p
			let mut z:u32 = 2;
			while jacobi_symbol(&BigUint::from(z), p).unwrap() != -1 {
					z = z+1;
			}
			
			// At this point z should be non-residue
			// Initialize m = s
			let mut m = s;
			// Initialize c = z^q
			let mut c = BigUint::modpow(&BigUint::from(z), &q, p);
			// Initialize t = a^q
			let mut t = BigUint::modpow(a, &q, p);
			// Initialize r = a^{(q+1)/2}
			let mut r = BigUint::modpow(a, &( (q + 1u32)/ 2u32 ), p);

			// We execute the Tonelli-Shanks loop until t = 1 % p
			while t != BigUint::one() {
				// Clone t
				let mut tt = t.clone();
				let mut i = 0; // Initialize i
			
				// We now successively square tt (mod p) until we get 
				// to 1
				while tt != BigUint::one() {
					// Create a temp variable to do swaps
					let mut tmp = (&tt * &tt) % p;
					swap(&mut tt, &mut tmp);
					i += 1;
				}
				if i == m{
					break;
				}	
				// Compute b = c**(2**(m-i-1)) mod p
				let b = BigUint::modpow(&c, &BigUint::from(1u32 << (m-i-1)), p);
				// Update all of the original variables, use a temp
				// variable to update the BigUints via swaps
				m = i;
				let mut tmp = (&b * &b) % p;
				swap(&mut c, &mut tmp);
				tmp = (&t * &c) % p;
				swap(&mut t, &mut tmp);
				tmp = (&r * &b) % p;
				swap(&mut r, &mut tmp);
			}
			// At this point, we have a candidate r, if it is a sqrt
			// great, otherwise return None
			match a.cmp(&((&r * &r) % p)) {
				Ordering::Equal => Some(r),
				_ => None
			}
		}	
	}		
}	


