#[macro_export]
macro_rules! forward_ref_val_binop {
	(impl $imp:ident for $res:ident, $method:ident) => {
		 impl<'a> $imp<$res> for &'a $res {
			 type Output = $res<$t>;
			 fn $method(self, other:$res) -> $res {
				 $imp::$method(self, &other)
			 }
		 }
	 }; 

	(impl $imp:ident for $res:ident<$t:tt>, 
	 $method:ident,
	 $bnd:ident $(+ $bnds:ident$(<$name:ident>)?)* ) => {
		 impl<'a,$t> $imp<$res<$t>> for &'a $res<$t>
		 where $t: $bnd $(+ $bnds$(<$name = $t>)?)* {
			 type Output = $res<$t>;
			 fn $method(self, other:$res<$t>) -> $res<$t> {
				 $imp::$method(self, &other)
			 }
		 }
	 } 
}

#[macro_export]
macro_rules! forward_val_ref_binop {
	(impl $imp:ident for $res:ident, $method:ident) => {
		 impl $imp<&$res> for $res {
			 type Output = $res<$t>;
			 fn $method(self, other:&$res) -> $res {
				 $imp::$method(&self, other)
			 }
		 }
	 }; 

	(impl $imp:ident for $res:ident<$t:tt>, 
	 $method:ident,
	 $bnd:ident $(+ $bnds:ident$(<$name:ident>)?)* ) => {
		 impl<$t> $imp<&$res<$t>> for $res<$t>
		 where $t: $bnd $(+ $bnds$(<$name = $t>)?)* {
			 type Output = $res<$t>;
			 fn $method(self, other:&$res<$t>) -> $res<$t> {
				 $imp::$method(&self, other)
			 }
		 }
	 } 
}

#[macro_export]
macro_rules! forward_val_val_binop {
	(impl $imp:ident for $res:ident, $method:ident) => {
		 impl $imp<$res> for $res {
			 type Output = $res<$t>;
			 fn $method(self, other:$res) -> $res {
				 $imp::$method(&self, &other)
			 }
		 }
	 }; 

	(impl $imp:ident for $res:ident<$t:tt>, 
	 $method:ident,
	 $bnd:ident $(+ $bnds:ident$(<$name:ident>)?)* ) => {
		 impl<$t> $imp<$res<$t>> for $res<$t>
		 where $t: $bnd $(+ $bnds$(<$name = $t>)?)* {
			 type Output = $res<$t>;
			 fn $method(self, other:$res<$t>) -> $res<$t> {
				 $imp::$method(&self, &other)
			 }
		 }
	 } 
}

#[macro_export]
macro_rules! forward_ref_scalar_ref_binop {
	(impl $imp:ident for $res:ident<$t:tt>, 
	 $method:ident,
	 $bnd:ident $(+ $bnds:ident$(<$name:ident>)?)* ) => {
		 impl<'a,'b,$t> $imp<&'b $t> for &'a $res<$t>
		 where $t: $bnd $(+ $bnds$(<$name = $t>)?)* {
			 type Output = $res<$t>;
			 fn $method(self, other:&$t) -> $res<$t> {
				 $imp::$method(self, &$res::from(other))
			 }
		 }
	 } 
}

#[macro_export]
macro_rules! forward_ref_scalar_val_binop {
	(impl $imp:ident for $res:ident<$t:tt>, 
	 $method:ident,
	 $bnd:ident $(+ $bnds:ident$(<$name:ident>)?)* ) => {
		 impl<'a,$t> $imp<$t> for &'a $res<$t>
		 where $t: $bnd $(+ $bnds$(<$name = $t>)?)* {
			 type Output = $res<$t>;
			 fn $method(self, other:$t) -> $res<$t> {
				 $imp::$method(self, &$res::from(&other))
			 }
		 }
	 } 
}

#[macro_export]
macro_rules! forward_val_scalar_ref_binop {
	(impl $imp:ident for $res:ident<$t:tt>, 
	 $method:ident,
	 $bnd:ident $(+ $bnds:ident$(<$name:ident>)?)* ) => {
		 impl<$t> $imp<&$t> for $res<$t>
		 where $t: $bnd $(+ $bnds$(<$name = $t>)?)* {
			 type Output = $res<$t>;
			 fn $method(self, other:&$t) -> $res<$t> {
				 $imp::$method(&self, &$res::from(other))
			 }
		 }
	 } 
}

#[macro_export]
macro_rules! forward_val_scalar_val_binop {
	(impl $imp:ident for $res:ident<$t:tt>, 
	 $method:ident,
	 $bnd:ident $(+ $bnds:ident$(<$name:ident>)?)* ) => {
		 impl<$t> $imp<$t> for $res<$t>
		 where $t: $bnd $(+ $bnds$(<$name = $t>)?)* {
			 type Output = $res<$t>;
			 fn $method(self, other:$t) -> $res<$t> {
				 $imp::$method(&self, &$res::from(&other))
			 }
		 }
	 } 
}

#[macro_export]
macro_rules! generate_binop_assign_ref {
	(impl $imp:ident for $res:ident<$t:tt>, 
	 $method:ident,
	 $op:tt,
	 $bnd:ident $(+ $bnds:ident$(<$name:ident>)?)* ) => {
		 impl<$t> $imp<&$res<$t>> for $res<$t>
		 where $t: $bnd $(+ $bnds$(<$name = $t>)?)* {
			 fn $method(&mut self, rhs:&$res<$t>) {
				 let n = replace(self, $res::zero());
				 *self = n $op rhs;
			 }
		 }
	 } 
}

#[macro_export]
macro_rules! generate_binop_assign_val {
	(impl $imp:ident for $res:ident<$t:tt>, 
	 $method:ident,
	 $op:tt,
	 $bnd:ident $(+ $bnds:ident$(<$name:ident>)?)* ) => {
		 impl<$t> $imp<$res<$t>> for $res<$t>
		 where $t: $bnd $(+ $bnds$(<$name = $t>)?)* {
			 fn $method(&mut self, rhs:$res<$t>) {
				 let n = replace(self, $res::zero());
				 *self = n $op &rhs;
			 }
		 }
	 } 
}

#[macro_export]
macro_rules! generate_binop_assign_scalar_ref {
	(impl $imp:ident for $res:ident<$t:tt>, 
	 $method:ident,
	 $op:tt,
	 $bnd:ident $(+ $bnds:ident$(<$name:ident>)?)* ) => {
		 impl<$t> $imp<&$t> for $res<$t>
		 where $t: $bnd $(+ $bnds$(<$name = $t>)?)* {
			 fn $method(&mut self, rhs:&$t) {
				 let n = replace(self, $res::zero());
				 *self = n $op &$res::from(rhs);
			 }
		 }
	 } 
}

#[macro_export]
macro_rules! generate_binop_assign_scalar_val {
	(impl $imp:ident for $res:ident<$t:tt>, 
	 $method:ident,
	 $op:tt,
	 $bnd:ident $(+ $bnds:ident$(<$name:ident>)?)* ) => {
		 impl<$t> $imp<$t> for $res<$t>
		 where $t: $bnd $(+ $bnds$(<$name = $t>)?)* {
			 fn $method(&mut self, rhs:$t) {
				 let n = replace(self, $res::zero());
				 *self = n $op &$res::from(&rhs);
			 }
		 }
	 } 
}
