#[allow(unused_imports)]
use num::traits::{One,Zero};
#[allow(unused_imports)]
use crate::rational;

#[test]
fn rational_display_test() {
	let a = rational::Rational::new(5,7);
	println!("{}", a);
	assert_eq!(&format!("{}",a),"5 / 7");
}

#[test]
fn rational_new_test() {
	let a = rational::Rational::new(10,14);
	assert!( (a.num == 5) && (a.denom == 7) );
}

#[test]
fn rational_add_test() {
	let a = rational::Rational::new(1,3);
	let b = rational::Rational::new(1,4);
	let c = rational::Rational::new(7,12);
	assert_eq!(a+b,c);
}

#[test]
fn rational_add_assign_test() {
	let mut a = rational::Rational::new(1,3);
	let b = rational::Rational::new(1,4);
	let c = rational::Rational::new(7,12);
	a += b;
	assert_eq!(a,c);
}

#[test]
fn rational_sub_test() {
	let a = rational::Rational::new(2,3);
	let b = rational::Rational::new(3,5);
	let c = rational::Rational::new(1,15);
	assert_eq!(a-b,c);
}

#[test]
fn rational_sub_assign_test() {
	let mut a = rational::Rational::new(2,3);
	let b = rational::Rational::new(3,5);
	let c = rational::Rational::new(1,15);
	a -= b;
	assert_eq!(a,c);
}

#[test]
fn rational_mul_test() {
	let a = rational::Rational::new(5,6);
	let b = rational::Rational::new(2,3);
	let c = rational::Rational::new(5,9);
	assert_eq!(a*b,c);
}

#[test]
fn rational_mul_assign_test() {
	let mut a = rational::Rational::new(5,6);
	let b = rational::Rational::new(2,3);
	let c = rational::Rational::new(5,9);
	a *= b;
	assert_eq!(a,c);
}

#[test]
fn rational_div_test() {
	let a = rational::Rational::new(5,6);
	let b = rational::Rational::new(2,3);
	let c = rational::Rational::new(5,4);
	assert_eq!(a/b,c);
}

#[test]
fn rational_div_assign_test() {
	let mut a = rational::Rational::new(5,6);
	let b = rational::Rational::new(2,3);
	let c = rational::Rational::new(5,4);
	a /= b;
	assert_eq!(a,c);
}

#[test]
fn rational_scalar_add_test() {
	let a = rational::Rational::new(1,5);
	let b = rational::Rational::new(16,5);
	assert_eq!(a+3,b);
}

#[test]
fn rational_scalar_add_assign_test() {
	let mut a = rational::Rational::new(1,5);
	let b = rational::Rational::new(16,5);
	a += 3;
	assert_eq!(a,b);
}

#[test]
fn rational_scalar_sub_test() {
	let a = rational::Rational::new(6,5);
	let b = rational::Rational::new(-4,5);
	assert_eq!(a-2,b);
}

#[test]
fn rational_scalar_sub_assign_test() {
	let mut a = rational::Rational::new(6,5);
	let b = rational::Rational::new(-4,5);
	a -= 2;
	assert_eq!(a,b);
}

#[test]
fn rational_scalar_mul_test() {
	let a = rational::Rational::new(11,20);
	let b = rational::Rational::new(33,4);
	assert_eq!(a*15,b);
}

#[test]
fn rational_scalar_mul_assign_test() {
	let mut a = rational::Rational::new(11,20);
	let b = rational::Rational::new(33,4);
	a *= 15;
	assert_eq!(a,b);
}

#[test]
fn rational_scalar_div_test() {
	let a = rational::Rational::new(55,7);
	let b = rational::Rational::new(11,21);
	assert_eq!(a/15,b);
}

#[test]
fn rational_scalar_mul_div_test() {
	let mut a = rational::Rational::new(55,7);
	let b = rational::Rational::new(11,21);
	a /= 15;
	assert_eq!(a,b);
}

#[test]
fn rational_recip_test() {
	let a = rational::Rational::new(3,4);
	let b = rational::Rational::new(4,3);
	assert_eq!(a.recip(),Some(b));
}

#[test]
fn rational_cont_fraction_test() {
	let a = rational::cont_fraction(&vec![1,1,1,1,1]);
	let b = rational::Rational::new(8,5);
	assert_eq!(a,b);
}

#[test]
fn rational_one_test() {
	let a: rational::Rational<i32> = One::one();
	let b = rational::Rational::new(1,1);
	assert_eq!(a,b);
}
