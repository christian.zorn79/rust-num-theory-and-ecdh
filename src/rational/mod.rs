use std::cmp::Eq;
use std::convert::From;
use std::mem::replace;
use std::ops::{Add,AddAssign,Div,DivAssign,Mul,MulAssign,Neg,Sub,SubAssign};
use std::fmt::{Debug,Display,Formatter,Result};
use num::integer::Integer;
use num::traits::{One,Zero};

mod test;

#[derive(Clone,Copy,Debug)]	
pub struct Rational<T> {
    pub num   : T,
    pub denom : T
}	

impl<T> PartialEq for Rational<T> where T:Copy+Eq+Integer {
	fn eq(&self, other: &Self) -> bool{
		(self.num*other.denom)==(self.denom*other.num)
	}
}

impl<T> Eq for Rational<T> where T:Copy + Eq + Integer{}

impl<T> Display for Rational<T> where T:Display+Integer{
	fn fmt(&self, f: &mut Formatter<'_>) -> Result {
		write!(f, "{} / {}", self.num, self.denom)
	}
}

macro_rules! forward_ref_val_binop_rational {
	(impl $imp:ident for $res:ident<$t:tt>, $method:ident) => {
		forward_ref_val_binop!(impl $imp for $res<$t>, 
		                 $method,
		                 Copy + Integer );
	}
}

macro_rules! forward_val_ref_binop_rational {
	(impl $imp:ident for $res:ident<$t:tt>, $method:ident) => {
		forward_val_ref_binop!(impl $imp for $res<$t>, 
		                 $method,
		                 Copy + Integer );
	}
}

macro_rules! forward_val_val_binop_rational {
	(impl $imp:ident for $res:ident<$t:tt>, $method:ident) => {
		forward_val_val_binop!(impl $imp for $res<$t>, 
		                 $method,
		                 Copy + Integer );
	}
}

macro_rules! forward_ref_scalar_ref_binop_rational {
	(impl $imp:ident for $res:ident<$t:tt>, $method:ident) => {
		forward_ref_scalar_ref_binop!(impl $imp for $res<$t>, 
		                 $method,
		                 Copy + Integer );
	}
}

macro_rules! forward_ref_scalar_val_binop_rational {
	(impl $imp:ident for $res:ident<$t:tt>, $method:ident) => {
		forward_ref_scalar_val_binop!(impl $imp for $res<$t>, 
		                 $method,
		                 Copy + Integer );
	}
}

macro_rules! forward_val_scalar_ref_binop_rational {
	(impl $imp:ident for $res:ident<$t:tt>, $method:ident) => {
		forward_val_scalar_ref_binop!(impl $imp for $res<$t>, 
		                 $method,
		                 Copy + Integer );
	}
}

macro_rules! forward_val_scalar_val_binop_rational {
	(impl $imp:ident for $res:ident<$t:tt>, $method:ident) => {
		forward_val_scalar_val_binop!(impl $imp for $res<$t>, 
		                 $method,
		                 Copy + Integer );
	}
}

macro_rules! generate_binop_assign_ref_rational {
	(impl $imp:ident for $res:ident<$t:tt>, $method:ident, $op:tt) => {
		generate_binop_assign_ref!(impl $imp for $res<$t>, 
		                 $method,
		                 $op,
		                 Copy + Integer + Zero );
	}
}

macro_rules! generate_binop_assign_val_rational {
	(impl $imp:ident for $res:ident<$t:tt>, $method:ident, $op:tt) => {
		generate_binop_assign_val!(impl $imp for $res<$t>, 
		                 $method,
		                 $op,
		                 Copy + Integer + Zero );
	}
}

macro_rules! generate_binop_assign_scalar_ref_rational {
	(impl $imp:ident for $res:ident<$t:tt>, $method:ident, $op:tt) => {
		generate_binop_assign_scalar_ref!(impl $imp for $res<$t>, 
		                 $method,
		                 $op,
		                 Copy + Integer + Zero );
	}
}

macro_rules! generate_binop_assign_scalar_val_rational {
	(impl $imp:ident for $res:ident<$t:tt>, $method:ident, $op:tt) => {
		generate_binop_assign_scalar_val!(impl $imp for $res<$t>, 
		                 $method,
		                 $op,
		                 Copy + Integer + Zero );
	}
}
	
impl<'a,'b, T> Add<&'b Rational<T>> for &'a Rational<T> where T:Copy+Integer{
	type Output = Rational<T>;
	fn add(self, other: &Rational<T>) -> Rational<T> {
		let num   = self.num*other.denom + self.denom*other.num;
		let denom = self.denom*other.denom;
		let g = Integer::gcd(&num,&denom);
		Rational{ num:num/g, denom:denom/g }
	}
}
forward_ref_val_binop_rational!(impl Add for Rational<T>, add);
forward_val_ref_binop_rational!(impl Add for Rational<T>, add);
forward_val_val_binop_rational!(impl Add for Rational<T>, add);
forward_ref_scalar_ref_binop_rational!(impl Add for Rational<T>, add);
forward_ref_scalar_val_binop_rational!(impl Add for Rational<T>, add);
forward_val_scalar_ref_binop_rational!(impl Add for Rational<T>, add);
forward_val_scalar_val_binop_rational!(impl Add for Rational<T>, add);
generate_binop_assign_ref_rational!(impl AddAssign for Rational<T>, add_assign, +);
generate_binop_assign_val_rational!(impl AddAssign for Rational<T>, add_assign, +);
generate_binop_assign_scalar_ref_rational!(impl AddAssign for Rational<T>, add_assign, +);
generate_binop_assign_scalar_val_rational!(impl AddAssign for Rational<T>, add_assign, +);

impl<'a,'b,T> Div<&'b Rational<T>> for &'a Rational<T> 
where T:Copy+Integer{
	type Output = Rational<T>;
	fn div(self, other: &Rational<T>) -> Rational<T> {
		let num   = self.num*other.denom;
		let denom = self.denom*other.num;
		let g = Integer::gcd(&num,&denom);
		Rational{ num:num/g, denom:denom/g }
	}
}
forward_ref_val_binop_rational!(impl Div for Rational<T>, div);
forward_val_ref_binop_rational!(impl Div for Rational<T>, div);
forward_val_val_binop_rational!(impl Div for Rational<T>, div);
forward_ref_scalar_ref_binop_rational!(impl Div for Rational<T>, div);
forward_ref_scalar_val_binop_rational!(impl Div for Rational<T>, div);
forward_val_scalar_ref_binop_rational!(impl Div for Rational<T>, div);
forward_val_scalar_val_binop_rational!(impl Div for Rational<T>, div);
generate_binop_assign_ref_rational!(impl DivAssign for Rational<T>, div_assign, /);
generate_binop_assign_val_rational!(impl DivAssign for Rational<T>, div_assign, /);
generate_binop_assign_scalar_ref_rational!(impl DivAssign for Rational<T>, div_assign, /);
generate_binop_assign_scalar_val_rational!(impl DivAssign for Rational<T>, div_assign, /);


impl<'a,'b,T> Mul<&'b Rational<T>> for &'a Rational<T> 
where T:Copy+Integer{
	type Output = Rational<T>;
	fn mul(self, other: &Rational<T>) -> Rational<T> {
		let num   = self.num*other.num;
		let denom = self.denom*other.denom;
		let g = Integer::gcd(&num,&denom);
		Rational{ num:num/g, denom:denom/g }
	}
}
forward_ref_val_binop_rational!(impl Mul for Rational<T>, mul);
forward_val_ref_binop_rational!(impl Mul for Rational<T>, mul);
forward_val_val_binop_rational!(impl Mul for Rational<T>, mul);
forward_ref_scalar_ref_binop_rational!(impl Mul for Rational<T>, mul);
forward_ref_scalar_val_binop_rational!(impl Mul for Rational<T>, mul);
forward_val_scalar_ref_binop_rational!(impl Mul for Rational<T>, mul);
forward_val_scalar_val_binop_rational!(impl Mul for Rational<T>, mul);
generate_binop_assign_ref_rational!(impl MulAssign for Rational<T>, mul_assign, *);
generate_binop_assign_val_rational!(impl MulAssign for Rational<T>, mul_assign, *);
generate_binop_assign_scalar_ref_rational!(impl MulAssign for Rational<T>, mul_assign, *);
generate_binop_assign_scalar_val_rational!(impl MulAssign for Rational<T>, mul_assign, *);

impl<'a,'b,T> Sub<&'b Rational<T>> for &'a Rational<T> 
where T:Copy+Integer{
	type Output = Rational<T>;
	fn sub(self, other: &Rational<T>) -> Rational<T> {
		let num   = self.num*other.denom - self.denom*other.num;
		let denom = self.denom*other.denom;
		let g = Integer::gcd(&num, &denom);
		Rational{ num:num/g, denom:denom/g }
	}
}
forward_ref_val_binop_rational!(impl Sub for Rational<T>, sub);
forward_val_ref_binop_rational!(impl Sub for Rational<T>, sub);
forward_val_val_binop_rational!(impl Sub for Rational<T>, sub);
forward_ref_scalar_ref_binop_rational!(impl Sub for Rational<T>, sub);
forward_ref_scalar_val_binop_rational!(impl Sub for Rational<T>, sub);
forward_val_scalar_ref_binop_rational!(impl Sub for Rational<T>, sub);
forward_val_scalar_val_binop_rational!(impl Sub for Rational<T>, sub);
generate_binop_assign_ref_rational!(impl SubAssign for Rational<T>, sub_assign, -);
generate_binop_assign_val_rational!(impl SubAssign for Rational<T>, sub_assign, -);
generate_binop_assign_scalar_ref_rational!(impl SubAssign for Rational<T>, sub_assign, -);
generate_binop_assign_scalar_val_rational!(impl SubAssign for Rational<T>, sub_assign, -);

impl<T> Neg for Rational<T> where T:Neg<Output=T>{
	type Output = Self;
	fn neg(self) -> Self {
		Rational{ num:-self.num, denom:self.denom }
	}
}

impl<T> One for Rational<T> where T:Copy+Integer+One {
	fn one() -> Self{
		Rational { num:T::one(), denom:T::one() }
	}
}	

impl<T> Zero for Rational<T> where T:Copy+Integer+Zero {
	fn zero() -> Self{
		Rational { num:T::zero(), denom:T::one() }
	}
	fn is_zero(&self) -> bool {
		self.num == T::zero()
	}
}
	
impl<T> From<T> for Rational<T> where T:Integer+One {
	fn from(int: T) -> Self {
		Rational { num:int, denom:T::one() }
	}
}		

impl<T> From<&T> for Rational<T> where T:Copy+Integer+One {
	fn from(int: &T) -> Self {
		Rational { num:int.clone(), denom:T::one() }
	}
}

impl<T> Rational<T> where T:Copy+Integer{
	pub fn recip(&self) -> Option<Self> {
		if self.is_zero(){
			None
		}
		else {
			Some(Rational{ num:self.denom, denom:self.num } )
		}
	}
}

impl<T> Rational<T> where T:Copy+Integer{
	pub fn new(a:T, b:T) -> Self{
		let g = Integer::gcd(&a,&b);
		Rational{  num:a/g, denom:b/g }
	}
}

pub fn cont_fraction<T>(cont_frac: &Vec<T>) -> Rational<T>
where T:Copy + Integer + One + Zero{
	let mut p0:T = T::zero();
	let mut p1:T = T::one();
	let mut q0:T = T::one();
	let mut q1:T = T::zero();
	for x in cont_frac{
		let p = p0 + *x*p1;
		let q = q0 + *x*q1;
		p0 = replace(&mut p1, p);
		q0 = replace(&mut q1, q);
	}
	Rational{ num:p1, denom:q1}
}
