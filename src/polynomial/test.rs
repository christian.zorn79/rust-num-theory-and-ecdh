#[allow(unused_imports)]
use num::traits::{One,Zero};
#[allow(unused_imports)]
use crate::polynomial;

#[test]
fn polynomial_display_test() {
	let a = polynomial::Polynomial{ coeffs:vec![2,3,5,7,11] };
	println!("{}", a);
	assert_eq!(&format!("{}",a),"2 + 3x + 5x^2 + 7x^3 + 11x^4");
}

#[test]
fn polynomial_new_test() {
	let a = polynomial::Polynomial::new( &vec![1,2,3,0,0] );
	let b = polynomial::Polynomial{ coeffs:vec![1,2,3] };
	assert_eq!(a,b);
}

#[test]
fn polynomial_zero_test() {
	let a: polynomial::Polynomial<i32> = polynomial::Polynomial::new(&vec![]);
	let b: polynomial::Polynomial<i32> = Zero::zero();
	assert_eq!(a,b);
}

#[test]
fn polynomial_add_test() {
	let a: polynomial::Polynomial<i32> = Zero::zero();
	let b = polynomial::Polynomial::new(&vec![1,2,3]);
	let c = b.clone();
	assert_eq!(a+b,c);
}

#[test]
fn polynomial_add_test2() {
	let a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = polynomial::Polynomial::new(&vec![2,3,5]);
	let c = polynomial::Polynomial::new(&vec![3,5,8]);
	assert_eq!(a+b,c);		
}

#[test]
fn polynomial_add_test3() {
	let a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = polynomial::Polynomial::new(&vec![2,3,5,7,11]);
	let c = polynomial::Polynomial::new(&vec![3,5,8,7,11]);
	assert_eq!(a+b,c);		
}

#[test]
fn polynomial_add_scalar_test() {
	let a: polynomial::Polynomial<i32> = Zero::zero();
	let b: i32 = 5;
	let c = polynomial::Polynomial::new(&vec![b]);
	assert_eq!(a+b,c);
}

#[test]
fn polynomial_add_scalar_test2() {
	let a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = 10;
	let c = polynomial::Polynomial::new(&vec![11,2,3]);
	assert_eq!(a+b,c);		
}

#[test]
fn polynomial_sub_test() {
	let a: polynomial::Polynomial<i32> = Zero::zero();
	let b = polynomial::Polynomial::new(&vec![1,-2,3]);
	let c = polynomial::Polynomial::new(&vec![-1,2,-3]);
	assert_eq!(a-b,c);
}

#[test]
fn polynomial_sub_test2() {
	let a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = polynomial::Polynomial::new(&vec![2,3,5]);
	let c = polynomial::Polynomial::new(&vec![-1,-1,-2]);
	assert_eq!(a-b,c);		
}

#[test]
fn polynomial_sub_test3() {
	let a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = polynomial::Polynomial::new(&vec![2,-3,5,-7,11]);
	let c = polynomial::Polynomial::new(&vec![-1,5,-2,7,-11]);
	assert_eq!(a-b,c);		
}

#[test]
fn polynomial_sub_scalar_test() {
	let a: polynomial::Polynomial<i32> = Zero::zero();
	let b: i32 = 5;
	let c = polynomial::Polynomial::new(&vec![-b]);
	assert_eq!(a-b,c);
}

#[test]
fn polynomial_sub_scalar_test2() {
	let a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = 10;
	let c = polynomial::Polynomial::new(&vec![-9,2,3]);
	assert_eq!(a-b,c);		
}

#[test]
fn polynomial_mul_test() {
	let a: polynomial::Polynomial<i32> = Zero::zero();
	let b = polynomial::Polynomial::new(&vec![1,2,3]);
	let c = a.clone();
	assert_eq!(a*b,c);
}

#[test]
fn polynomial_mul_test2() {
	let a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = polynomial::Polynomial::new(&vec![2,3,5]);
	let c = polynomial::Polynomial::new(&vec![2,7,17,19,15]);
	assert_eq!(a*b,c);		
}

#[test]
fn polynomial_mul_test3() {
	let a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = polynomial::Polynomial::new(&vec![2,3,5,7,11]);
	let c = polynomial::Polynomial::new(&vec![2,7,17,26,40,43,33]);
	assert_eq!(a*b,c);		
}

#[test]
fn polynomial_mul_scalar_test() {
	let a: polynomial::Polynomial<i32> = Zero::zero();
	let b: i32 = 5;
	let c = a.clone();
	assert_eq!(a*b,c);
}

#[test]
fn polynomial_mul_scalar_test2() {
	let a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = 5;
	let c = polynomial::Polynomial::new(&vec![5,10,15]);
	assert_eq!(a*b,c);		
}

#[test]
fn polynomial_zero_test2() {
	let a: polynomial::Polynomial<i32> = polynomial::Polynomial::new(&vec![]);
	let b: polynomial::Polynomial<i32> = Zero::zero();
	assert_eq!(a,b);
}

#[test]
fn polynomial_add_assign_test() {
	let mut a: polynomial::Polynomial<i32> = Zero::zero();
	let b = polynomial::Polynomial::new(&vec![1,2,3]);
	let c = b.clone();
	a += b;
	assert_eq!(a,c);
}

#[test]
fn polynomial_add_assign_test2() {
	let mut a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = polynomial::Polynomial::new(&vec![2,3,5]);
	let c = polynomial::Polynomial::new(&vec![3,5,8]);
	a += b;
	assert_eq!(a,c);		
}

#[test]
fn polynomial_add_assign_test3() {
	let mut a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = polynomial::Polynomial::new(&vec![2,3,5,7,11]);
	let c = polynomial::Polynomial::new(&vec![3,5,8,7,11]);
	a += b;
	assert_eq!(a,c);		
}

#[test]
fn polynomial_add_assign_scalar_test() {
	let mut a: polynomial::Polynomial<i32> = Zero::zero();
	let b: i32 = 5;
	let c = polynomial::Polynomial::new(&vec![b]);
	a += b;
	assert_eq!(a,c);
}

#[test]
fn polynomial_add_assign_scalar_test2() {
	let mut a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = 10;
	let c = polynomial::Polynomial::new(&vec![11,2,3]);
	a += b;
	assert_eq!(a,c);		
}

#[test]
fn polynomial_sub_assign_test() {
	let mut a: polynomial::Polynomial<i32> = Zero::zero();
	let b : polynomial::Polynomial<i32> = polynomial::Polynomial::new(&vec![1,-2,3]);
	let c : polynomial::Polynomial<i32> = polynomial::Polynomial::new(&vec![-1,2,-3]);
	a -= b;
	assert_eq!(a,c);
}

#[test]
fn polynomial_sub_assign_test2() {
	let mut a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = polynomial::Polynomial::new(&vec![2,3,5]);
	let c = polynomial::Polynomial::new(&vec![-1,-1,-2]);
	a -= b;
	assert_eq!(a,c);		
}

#[test]
fn polynomial_sub_assign_test3() {
	let mut a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = polynomial::Polynomial::new(&vec![2,-3,5,-7,11]);
	let c = polynomial::Polynomial::new(&vec![-1,5,-2,7,-11]);
	a -= b;
	assert_eq!(a,c);		
}

#[test]
fn polynomial_sub_assign_scalar_test() {
	let mut a: polynomial::Polynomial<i32> = Zero::zero();
	let b: i32 = 5;
	let c = polynomial::Polynomial::new(&vec![-b]);
	a -= b;
	assert_eq!(a,c);
}

#[test]
fn polynomial_sub_assign_scalar_test2() {
	let mut a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = 10;
	let c = polynomial::Polynomial::new(&vec![-9,2,3]);
	a -= b;
	assert_eq!(a,c);		
}

#[test]
fn polynomial_mul_assign_test() {
	let mut a: polynomial::Polynomial<i32> = Zero::zero();
	let b = polynomial::Polynomial::new(&vec![1,2,3]);
	let c = a.clone();
	a *= b;
	assert_eq!(a,c);
}

#[test]
fn polynomial_mul_assign_test2() {
	let mut a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = polynomial::Polynomial::new(&vec![2,3,5]);
	let c = polynomial::Polynomial::new(&vec![2,7,17,19,15]);
	a *= b;
	assert_eq!(a,c);		
}

#[test]
fn polynomial_mul_assign_test3() {
	let mut a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = polynomial::Polynomial::new(&vec![2,3,5,7,11]);
	let c = polynomial::Polynomial::new(&vec![2,7,17,26,40,43,33]);
	a *= b;
	assert_eq!(a,c);		
}

#[test]
fn polynomial_mul_assign_scalar_test() {
	let mut a: polynomial::Polynomial<i32> = Zero::zero();
	let b: i32 = 5;
	let c = a.clone();
	a *= b;
	assert_eq!(a,c);
}

#[test]
fn polynomial_mul_assign_scalar_test2() {
	let mut a = polynomial::Polynomial::new(&vec![1,2,3]);
	let b = 5;
	let c = polynomial::Polynomial::new(&vec![5,10,15]);
	a *= b;
	assert_eq!(a,c);		
}

#[test]
fn polynomial_deriv_test() {
	let a = polynomial::Polynomial::new(&vec![1,2,3,4,5]);
	let b = polynomial::Polynomial::new(&vec![2,6,12,20]);
	assert_eq!(a.deriv(),b);
}

#[test]
fn polynomial_trim_test() {
	let mut a = polynomial::Polynomial{ coeffs:vec![2,3,5,7,0,0] };
	let b = polynomial::Polynomial::new(&vec![2,3,5,7]);
	a.trim();
	assert_eq!(a,b);
}

#[test]
fn polynomial_one_test() {
	let a: polynomial::Polynomial<i32> = One::one();
	let b = polynomial::Polynomial::new(&vec![1]);
	assert_eq!(a,b);
}

