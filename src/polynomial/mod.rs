use std::cmp::Eq;
use std::convert::From;
use std::ops::{Add,AddAssign,Mul,MulAssign,Neg,Sub,SubAssign};
use std::fmt::{Debug,Display,Formatter,Result};
use std::mem::replace;
use std::vec::Vec;
use num::traits::{One,Zero};

mod test;

#[derive(Clone,Debug)]
pub struct Polynomial<T> {
	pub coeffs: Vec<T>
}

impl<'a,'b,T> Add<&'b Polynomial<T>> for &'a Polynomial<T>
where T:Add<Output=T> + Copy + Eq + Zero{
	type Output = Polynomial<T>;
	fn add(self, other: &Polynomial<T>) -> Polynomial<T>{
		let mut coeffs = Vec::new();
		let mut i = 0;
		loop {
			match (self.coeffs.get(i), other.coeffs.get(i)) {
				(Some(&a),Some(&b)) => coeffs.push(a+b),
				(Some(&a), None)    => coeffs.push(a),
				(None, Some(&b))    => coeffs.push(b),
				(None, None)        => {
					while coeffs.last() == Some(&T::zero()){
						coeffs.pop();
					}
					break Polynomial{ coeffs }
				}
			};
			i += 1;
		}
	}
}
forward_ref_val_binop!(impl Add for Polynomial<T>, add, 
  Copy + Eq + Zero + Add<Output>);
forward_val_ref_binop!(impl Add for Polynomial<T>, add, 
  Copy + Eq + Zero + Add<Output>);
forward_val_val_binop!(impl Add for Polynomial<T>, add, 
  Copy + Eq + Zero + Add<Output>);
forward_ref_scalar_ref_binop!(impl Add for Polynomial<T>, add, 
  Copy + Eq + Zero + Add<Output>);
forward_ref_scalar_val_binop!(impl Add for Polynomial<T>, add, 
  Copy + Eq + Zero + Add<Output>);
forward_val_scalar_ref_binop!(impl Add for Polynomial<T>, add, 
  Copy + Eq + Zero + Add<Output>);
forward_val_scalar_val_binop!(impl Add for Polynomial<T>, add, 
  Copy + Eq + Zero + Add<Output>);
generate_binop_assign_ref!(impl AddAssign for Polynomial<T>, add_assign, +, 
  Copy + Eq + Zero + Add<Output>);
generate_binop_assign_val!(impl AddAssign for Polynomial<T>, add_assign, +, 
  Copy + Eq + Zero + Add<Output>);
generate_binop_assign_scalar_ref!(impl AddAssign for Polynomial<T>, add_assign, +, 
  Copy + Eq + Zero + Add<Output>);
generate_binop_assign_scalar_val!(impl AddAssign for Polynomial<T>, add_assign, +, 
  Copy + Eq + Zero + Add<Output>);


impl<'a,'b, T> Sub<&'b Polynomial<T>> for &'a Polynomial<T>
where T:Sub<Output=T> + Copy + Eq + Neg<Output=T> + Zero{
	type Output = Polynomial<T>;
	fn sub(self, other: &Polynomial<T>) -> Polynomial<T>{
		let mut coeffs = Vec::new();
		let mut i = 0;
		loop {
			match (self.coeffs.get(i), other.coeffs.get(i)) {
				(Some(&a),Some(&b)) => coeffs.push(a-b),
				(Some(&a), None)    => coeffs.push(a),
				(None, Some(&b))    => coeffs.push(-b),
				(None, None)        => {
					while coeffs.last() == Some(&T::zero()){
						coeffs.pop();
					}
					break Polynomial{ coeffs }
				}
			};
			i += 1;
		}
	}
}
forward_ref_val_binop!(impl Sub for Polynomial<T>, sub, 
  Copy + Eq + Zero + Sub<Output> + Neg<Output>);
forward_val_ref_binop!(impl Sub for Polynomial<T>, sub, 
  Copy + Eq + Zero + Sub<Output> + Neg<Output>);
forward_val_val_binop!(impl Sub for Polynomial<T>, sub, 
  Copy + Eq + Zero + Sub<Output> + Neg<Output>);
forward_ref_scalar_ref_binop!(impl Sub for Polynomial<T>, sub, 
  Copy + Eq + Zero + Sub<Output> + Neg<Output>);
forward_ref_scalar_val_binop!(impl Sub for Polynomial<T>, sub, 
  Copy + Eq + Zero + Sub<Output> + Neg<Output>);
forward_val_scalar_ref_binop!(impl Sub for Polynomial<T>, sub, 
  Copy + Eq + Zero + Sub<Output> + Neg<Output>);
forward_val_scalar_val_binop!(impl Sub for Polynomial<T>, sub, 
  Copy + Eq + Zero + Sub<Output> + Neg<Output>);
generate_binop_assign_ref!(impl SubAssign for Polynomial<T>, sub_assign, -, 
  Copy + Eq + Zero + Sub<Output> + Neg<Output>);
generate_binop_assign_val!(impl SubAssign for Polynomial<T>, sub_assign, -, 
  Copy + Eq + Zero + Sub<Output> + Neg<Output>);
generate_binop_assign_scalar_ref!(impl SubAssign for Polynomial<T>, sub_assign, -, 
  Copy + Eq + Zero + Sub<Output> + Neg<Output>);
generate_binop_assign_scalar_val!(impl SubAssign for Polynomial<T>, sub_assign, -, 
  Copy + Eq + Zero + Sub<Output> + Neg<Output>);

impl<'a,'b, T> Mul<&'b Polynomial<T>> for &'a Polynomial<T>
where T:AddAssign + Mul<Output=T> + Copy + Eq + Zero{
	type Output = Polynomial<T>;
	fn mul(self, other: &Polynomial<T>) -> Polynomial<T> {
		let mut coeffs = Vec::new();
		let mut i = 0;
		while let Some(&a) = self.coeffs.get(i){
			let mut j = 0;
			while let Some(&b) = other.coeffs.get(j){
				if i+j < coeffs.len() {
					coeffs[i+j] += a*b;
				}
				else{
					coeffs.push(a*b);
				}
				j += 1;
			}
			i += 1;
		}
		while coeffs.last() == Some(&T::zero()) {
			coeffs.pop();
		}
		Polynomial{ coeffs }
	}
}
forward_ref_val_binop!(impl Mul for Polynomial<T>, mul, 
  Copy + Eq + Zero + AddAssign + Mul<Output>);
forward_val_ref_binop!(impl Mul for Polynomial<T>, mul, 
  Copy + Eq + Zero + AddAssign + Mul<Output>);
forward_val_val_binop!(impl Mul for Polynomial<T>, mul, 
  Copy + Eq + Zero + AddAssign + Mul<Output>);
forward_ref_scalar_ref_binop!(impl Mul for Polynomial<T>, mul, 
    Copy + Eq + Zero + AddAssign + Mul<Output>);
forward_ref_scalar_val_binop!(impl Mul for Polynomial<T>, mul, 
  Copy + Eq + Zero + AddAssign + Mul<Output>);
forward_val_scalar_ref_binop!(impl Mul for Polynomial<T>, mul, 
  Copy + Eq + Zero + AddAssign + Mul<Output>);
forward_val_scalar_val_binop!(impl Mul for Polynomial<T>, mul, 
  Copy + Eq + Zero + AddAssign + Mul<Output>);
generate_binop_assign_ref!(impl MulAssign for Polynomial<T>, mul_assign, *, 
  Copy + Eq + Zero + AddAssign + Mul<Output>);
generate_binop_assign_val!(impl MulAssign for Polynomial<T>, mul_assign, *, 
  Copy + Eq + Zero + AddAssign + Mul<Output>);
generate_binop_assign_scalar_ref!(impl MulAssign for Polynomial<T>, mul_assign, *, 
  Copy + Eq + Zero + AddAssign + Mul<Output>);
generate_binop_assign_scalar_val!(impl MulAssign for Polynomial<T>, mul_assign, *, 
  Copy + Eq + Zero + AddAssign + Mul<Output>);


impl<T> Zero for Polynomial<T>
where T:Copy + Eq + Zero{
	fn zero() -> Self {
		Polynomial{ coeffs:vec![] }
	}
	fn is_zero(&self) -> bool {
		self.coeffs == vec![]
	}
}  

impl<T> One for Polynomial<T>
where T:AddAssign +Copy + Eq + One + Zero{
	fn one() -> Self {
		Polynomial{ coeffs:vec![T::one()] }
	}
}

impl<T> From<T> for Polynomial<T>
where T: Copy + Eq + Zero {
	fn from(base: T) -> Self{
		let mut coeffs = vec![base];
		while coeffs.last() == Some(&T::zero()){
			coeffs.pop();
		}
		Polynomial{ coeffs }
	}
}

impl<T> From<&T> for Polynomial<T>
where T: Copy + Eq + Zero {
	fn from(base: &T) -> Self{
		let mut coeffs = vec![base.clone()];
		while coeffs.last() == Some(&T::zero()){
			coeffs.pop();
		}
		Polynomial{ coeffs }
		
	}
}

impl<T> PartialEq for Polynomial<T> where T:Copy+Eq {
	fn eq(&self, other: &Self) -> bool{
		self.coeffs == other.coeffs
	}
}

impl<T> Display for Polynomial<T> where T:Copy + Display + Eq + Zero {
	fn fmt(&self, f: &mut Formatter<'_>) -> Result {
		if self.is_zero(){
			write!(f, "0")
		}
		else{
			let mut i = 0;
			while let Some(&c) = self.coeffs.get(i){
				match i {
					0 => write!(f, "{}", c)?,
					1 => write!(f, " + {}x", c)?,
					_ => write!(f, " + {}x^{}", c, i)?
				};
				i += 1;
			}
			Ok(())
		}		
	}
}


impl<T> Polynomial<T> {
	pub fn degree(&self) -> i32 {
		(self.coeffs.len() as i32) - 1
	}
}

impl<T> Polynomial<T>
where T: AddAssign + MulAssign + Copy + Eq + Zero {
	pub fn eval(&self, base:T) -> T {
		let mut out = T::zero();
		let mut i = self.coeffs.len();
		while let Some(&c) = self.coeffs.get(i) {
			out *= base;
			out += c;
			i -= 1;
		}
		out
	}
}

impl<T> Polynomial<T>
where T: Mul<Output=T> + From<usize> + Copy + Eq + Zero{
	pub fn deriv(&self) -> Self{
		if self.degree() < 1 {
			Polynomial::zero()
		}
		else{
			let mut coeffs = Vec::new();
			let mut i = 1_usize;
			while let Some(&c) =  self.coeffs.get(i){
				coeffs.push(c * From::from(i));
				i += 1;
			}
			while coeffs.last() == Some(&T::zero()){
				coeffs.pop();
			}
			Polynomial{ coeffs }
		}
	}
}

impl<T> Polynomial<T>
where T:Copy + Eq + Zero {
	pub fn new(coeff_vec: &Vec<T>) -> Self {
		let mut coeffs = coeff_vec.clone();
		while coeffs.last() == Some(&T::zero()) {
			coeffs.pop();
		}
		Polynomial{ coeffs }
	}
	
	pub fn trim(&mut self){
		while self.coeffs.last() == Some(&T::zero()) {
			self.coeffs.pop();
		}
	}
}

