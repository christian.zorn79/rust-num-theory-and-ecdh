#[allow(unused_imports)]
use num_bigint::BigUint;
#[allow(unused_imports)]
use crate::modular_ring::ModRing;

#[test]
fn mod_ring_add_test() {
	let m = BigUint::from(10_u32);
	let a = ModRing{ elem:BigUint::from(5_u32), modulus:&m };
	let b = ModRing{ elem:BigUint::from(7_u32), modulus:&m };
	let c = ModRing{ elem:BigUint::from(2_u32), modulus:&m };
	assert_eq!(a+b,c);
}

#[test]
fn mod_ring_add_biguint_test() {
	let m = BigUint::from(10_u32);
	let a = ModRing{ elem:BigUint::from(5_u32), modulus:&m };
	let b = BigUint::from(7_u32);
	let c = ModRing{ elem:BigUint::from(2_u32), modulus:&m };
	assert_eq!(a+b,c);
}

#[test]
fn mod_ring_add_assign_test() {
	let m = BigUint::from(16_u32);
	let mut a = ModRing{ elem:BigUint::from(11_u32), modulus:&m };
	let b = ModRing{ elem:BigUint::from(13_u32), modulus:&m };
	let c = ModRing{ elem:BigUint::from(8_u32), modulus:&m };
	a += b;
	assert_eq!(a,c);
}

#[test]
fn mod_ring_add_assign_biguint_test() {
	let m = BigUint::from(16_u32);
	let mut a = ModRing{ elem:BigUint::from(11_u32), modulus:&m };
	let b = BigUint::from(13_u32);
	let c = ModRing{ elem:BigUint::from(8_u32), modulus:&m };
	a += b;
	assert_eq!(a,c);
}


#[test]
fn mod_ring_sub_test() {
	let m = BigUint::from(10_u32);
	let a = ModRing{ elem:BigUint::from(5_u32), modulus:&m };
	let b = ModRing{ elem:BigUint::from(7_u32), modulus:&m };
	let c = ModRing{ elem:BigUint::from(8_u32), modulus:&m };
	assert_eq!(a-b,c);
}

#[test]
fn mod_ring_sub_biguint_test() {
	let m = BigUint::from(10_u32);
	let a = ModRing{ elem:BigUint::from(5_u32), modulus:&m };
	let b = BigUint::from(7_u32);
	let c = ModRing{ elem:BigUint::from(8_u32), modulus:&m };
	assert_eq!(a-b,c);
}

#[test]
fn mod_ring_sub_assign_test() {
	let m = BigUint::from(20_u32);
	let mut a = ModRing{ elem:BigUint::from(11_u32), modulus:&m };
	let b = ModRing{ elem:BigUint::from(18_u32), modulus:&m };
	let c = ModRing{ elem:BigUint::from(13_u32), modulus:&m };
	a -= b;
	assert_eq!(a,c);
}

#[test]
fn mod_ring_sub_assign_biguint_test() {
	let m = BigUint::from(20_u32);
	let mut a = ModRing{ elem:BigUint::from(11_u32), modulus:&m };
	let b = BigUint::from(18_u32);
	let c = ModRing{ elem:BigUint::from(13_u32), modulus:&m };
	a -= b;
	assert_eq!(a,c);
}

#[test]
fn mod_ring_mul_test() {
	let m = BigUint::from(31_u32);
	let a = ModRing{ elem:BigUint::from(13_u32), modulus:&m };
	let b = ModRing{ elem:BigUint::from(15_u32), modulus:&m };
	let c = ModRing{ elem:BigUint::from(9_u32), modulus:&m };
	assert_eq!(a*b,c);
}

#[test]
fn mod_ring_mul_biguint_test() {
	let m = BigUint::from(31_u32);
	let a = ModRing{ elem:BigUint::from(13_u32), modulus:&m };
	let b = BigUint::from(15_u32);
	let c = ModRing{ elem:BigUint::from(9_u32), modulus:&m };
	assert_eq!(a*b,c);
}

#[test]
fn mod_ring_mul_assign_test() {
	let m = BigUint::from(17_u32);
	let mut a = ModRing{ elem:BigUint::from(10_u32), modulus:&m };
	let b = ModRing{ elem:BigUint::from(14_u32), modulus:&m };
	let c = ModRing{ elem:BigUint::from(4_u32), modulus:&m };
	a *= b;
	assert_eq!(a,c);
}

#[test]
fn mod_ring_mul_assign_biguint_test() {
	let m = BigUint::from(17_u32);
	let mut a = ModRing{ elem:BigUint::from(10_u32), modulus:&m };
	let b = BigUint::from(14_u32);
	let c = ModRing{ elem:BigUint::from(4_u32), modulus:&m };
	a *= b;
	assert_eq!(a,c);
}
