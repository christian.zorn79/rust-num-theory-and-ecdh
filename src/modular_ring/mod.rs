use num_bigint::BigUint;
use std::ops::{Add, AddAssign, Sub, SubAssign, Mul, MulAssign};

mod test;

#[derive(Clone,Debug)]
pub struct ModRing<'a> {
	elem: BigUint,
	modulus: &'a BigUint
}

impl PartialEq for ModRing<'_>{
	fn eq(&self, other: &Self) -> bool{
		(self.elem == other.elem) && (self.modulus == other.modulus)
	}
}

impl Add for ModRing<'_> {
	type Output = Self;
	fn add(self, other: Self) -> Self{
		if self.modulus != other.modulus {
			panic!("Conflicting moduli for ModRing Add: {} vs. {}", self.modulus, other.modulus);
		}
		ModRing{ elem:(self.elem + other.elem) % self.modulus, modulus:self.modulus }
	}
}

impl Add<BigUint> for ModRing<'_> {
	type Output = Self;
	fn add(self, other: BigUint) -> Self{
		ModRing{ elem:(self.elem + other) % self.modulus, modulus:self.modulus }
	}
}

impl AddAssign for ModRing<'_> {
	fn add_assign(&mut self, rhs: Self) {
		if self.modulus != rhs.modulus {
			panic!("Conflicting moduli for ModRing AddAssign: {} vs. {}", self.modulus, rhs.modulus);
		}
		let lhs = self.elem.clone();
		self.elem = (lhs + &rhs.elem) % self.modulus; 
	}
}

impl AddAssign<BigUint> for ModRing<'_> {
	fn add_assign(&mut self, rhs: BigUint) {
		let lhs = self.elem.clone();
		self.elem = (lhs + &rhs) % self.modulus;
	}
}

impl Sub for ModRing<'_> {
	type Output = Self;
	fn sub(self, other: Self) -> Self{
		if self.modulus != other.modulus {
			panic!("Conflicting moduli for ModRing Add: {} vs. {}", self.modulus, other.modulus);
		}
		ModRing{ elem:(self.elem + self.modulus - other.elem) % self.modulus, modulus:self.modulus }
	}
}

impl Sub<BigUint> for ModRing<'_> {
	type Output = Self;
	fn sub(self, other: BigUint) -> Self{
		ModRing{ elem:(self.elem + self.modulus - other) % self.modulus, modulus:self.modulus }
	}
}

impl SubAssign for ModRing<'_> {
	fn sub_assign(&mut self, rhs: Self) {
		if self.modulus != rhs.modulus {
			panic!("Conflicting moduli for ModRing SubAssign: {} vs. {}", self.modulus, rhs.modulus);
		}
		let lhs = self.elem.clone();
		self.elem = (lhs + self.modulus - &rhs.elem) % self.modulus; 
	}
}

impl SubAssign<BigUint> for ModRing<'_> {
	fn sub_assign(&mut self, rhs: BigUint) {
		let lhs = self.elem.clone();
		self.elem = (lhs + self.modulus - &rhs) % self.modulus;
	}
}

impl Mul for ModRing<'_> {
	type Output = Self;
	fn mul(self, other: Self) -> Self{
		if self.modulus != other.modulus {
			panic!("Conflicting moduli for ModRing Add: {} vs. {}", self.modulus, other.modulus);
		}
		ModRing{ elem:(self.elem * other.elem) % self.modulus, modulus:self.modulus }
	}
}

impl Mul<BigUint> for ModRing<'_> {
	type Output = Self;
	fn mul(self, other: BigUint) -> Self{
		ModRing{ elem:(self.elem * other) % self.modulus, modulus:self.modulus }
	}
}

impl MulAssign for ModRing<'_> {
	fn mul_assign(&mut self, rhs: Self) {
		if self.modulus != rhs.modulus {
			panic!("Conflicting moduli for ModRing AddAssign: {} vs. {}", self.modulus, rhs.modulus);
		}
		let lhs = self.elem.clone();
		self.elem = (lhs * &rhs.elem) % self.modulus; 
	}
}

impl MulAssign<BigUint> for ModRing<'_> {
	fn mul_assign(&mut self, rhs: BigUint) {
		let lhs = self.elem.clone();
		self.elem = (lhs * &rhs) % self.modulus;
	}
}

impl<'a> ModRing<'a> {
	pub fn set_modulus(&mut self, modulus: &'a BigUint){
		self.modulus = modulus;
	}
}

