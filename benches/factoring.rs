use num_bigint::BigUint;
#[macro_use]
extern crate bencher;
use bencher::Bencher;

use number_theory::utils;

fn modpow_bench(bench: &mut Bencher){
	let n  = BigUint::parse_bytes(b"a29eb632f52cd7f267e3561b9762723b9ead8ec2ea911ac79be0409681def54d26d02a596fe85f3b3e3a60a917cab6124d3c875f60b260212dd9e1068872b4aad7fadf2374b14dab118995273b7726453d5f093d1e67980207c31f8fda3c9464d09eb4f15c8128023551e26dc3021b184ec724b7a2120038b97ce99fba556275", 16).unwrap();
	let d  = BigUint::parse_bytes(b"7bf6c706ce1ce507d4444846e48f8cb37506b24e28d358624d9f0b7d5f12334884e39e1ef69381f49b76d697bd39ffb4b5f4c09041f3239594c16799dc19d933ba9443670f3c2115f3c1ab0d3c1249471e45ad8e7791075c3d128d0182ee59b448ff04e402564d3b0bc5306ae61ed3d02d6b2b19d5042b220c36392014808647", 16).unwrap();
    let a  = BigUint::from(0xdeadbeeffeedface_u64);
    bench.iter ( || {
		a.modpow(&d,&n);
	})
}

fn powmod_oddmod_bench(bench: &mut Bencher){
	let n  = BigUint::parse_bytes(b"a29eb632f52cd7f267e3561b9762723b9ead8ec2ea911ac79be0409681def54d26d02a596fe85f3b3e3a60a917cab6124d3c875f60b260212dd9e1068872b4aad7fadf2374b14dab118995273b7726453d5f093d1e67980207c31f8fda3c9464d09eb4f15c8128023551e26dc3021b184ec724b7a2120038b97ce99fba556275", 16).unwrap();
	let d  = BigUint::parse_bytes(b"7bf6c706ce1ce507d4444846e48f8cb37506b24e28d358624d9f0b7d5f12334884e39e1ef69381f49b76d697bd39ffb4b5f4c09041f3239594c16799dc19d933ba9443670f3c2115f3c1ab0d3c1249471e45ad8e7791075c3d128d0182ee59b448ff04e402564d3b0bc5306ae61ed3d02d6b2b19d5042b220c36392014808647", 16).unwrap();
    let a  = BigUint::from(0xdeadbeeffeedface_u64);
    bench.iter ( || {
		utils::powmod_oddmod(&a,&d,&n).unwrap();
	})
}

benchmark_group!(benches, modpow_bench, powmod_oddmod_bench);
benchmark_main!(benches);
