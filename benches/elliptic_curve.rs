use num_bigint::BigUint;
#[macro_use]
extern crate bencher;
use bencher::Bencher;

use number_theory::elliptic_curve::{EcPoint, ec_double, ec_add, ec_scalar_multiply};

fn ec_double_bench(bench: &mut Bencher){
	let p = BigUint::parse_bytes(b"39402006196394479212279040100143613805079739270465446667948293404245721771496870329047266088258938001861606973112319",10).unwrap();
	let a = &p - 3_u32;
	let x = EcPoint( BigUint::parse_bytes(b"aa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7", 16).unwrap(),
					 BigUint::parse_bytes(b"3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f", 16).unwrap(),
					 false);
	bench.iter( || {
		ec_double(&x, &a, &p).unwrap();
	})
}

fn ec_add_bench(bench: &mut Bencher){
	let p = BigUint::parse_bytes(b"39402006196394479212279040100143613805079739270465446667948293404245721771496870329047266088258938001861606973112319",10).unwrap();
	let a = &p - 3_u32;
	let x = EcPoint( BigUint::parse_bytes(b"aa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7", 16).unwrap(),
					 BigUint::parse_bytes(b"3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f", 16).unwrap(),
					 false);
	let y = EcPoint( BigUint::parse_bytes(b"08D999057BA3D2D969260045C55B97F089025959A6F434D651D207D19FB96E9E4FE0E86EBE0E64F85B96A9C75295DF61", 16).unwrap(),
	                 BigUint::parse_bytes(b"8E80F1FA5B1B3CEDB7BFE8DFFD6DBA74B275D875BC6CC43E904E505F256AB4255FFD43E94D39E22D61501E700A940E80", 16).unwrap(),
	                 false);
	bench.iter( || {
		ec_add(&x, &y, &a, &p).unwrap();
	})
}

fn ec_scalar_multiply_bench(bench: &mut Bencher){
	let p = BigUint::parse_bytes(b"39402006196394479212279040100143613805079739270465446667948293404245721771496870329047266088258938001861606973112319",10).unwrap();
	let a = &p - 3_u32;
	let x = EcPoint( BigUint::parse_bytes(b"aa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7", 16).unwrap(),
					 BigUint::parse_bytes(b"3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f", 16).unwrap(),
					 false);
	let n = BigUint::parse_bytes(b"39402006196394479212279040100143613805079739270465446667946905279627659399113263569398956308152294913554433653942642", 10).unwrap();
	bench.iter( || {
		ec_scalar_multiply(&x, &n, &a, &p).unwrap();
	})	
}

benchmark_group!(benches, ec_double_bench, ec_add_bench, ec_scalar_multiply_bench);
benchmark_main!(benches);
