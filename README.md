This project implements some basic number theory functions with the
goal of implementing ECDH using the NIST curves.  This is a project
associated with the author's goal of learning to program in Rust.
